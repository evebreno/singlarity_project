﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace NameThor
{
    public class StandardEncoder
    {
        static public byte[] GetBytes(string valueToGet)
        {
            return Encoding.Default.GetBytes(valueToGet);
        }
        static public byte[] GetBytes(int valueToGet)
        {
            return Encoding.Default.GetBytes(valueToGet.ToString());
        }
        static public byte[] GetBytes(float valueToGet)
        {
            return Encoding.Default.GetBytes(valueToGet.ToString());
        }
        static public byte GetByte(char valueToGet)
        {
            byte b = Encoding.Default.GetBytes(new char[] { valueToGet })[0];
            return b;
        }

        static public int GetInt(byte[] value)
        {
            return int.Parse(Encoding.Default.GetString(value));
        }
        static public float GetFloat(byte[] value)
        {
            return float.Parse(Encoding.Default.GetString(value));
        }
        static public string GetString(byte[] value)
        {
            return Encoding.Default.GetString(value);
        }
        static public char GetChar(byte value)
        {
            return Encoding.Default.GetChars(new byte[] { value })[0];
        }
    }
}