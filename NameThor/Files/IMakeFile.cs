﻿namespace NameThor
{
    public interface IMakeFile
    {
        byte[] Output { get; }

        void InsertByte(byte byteToInsert);
        void InsertBytes(byte[] byteToInsert);
    }
}