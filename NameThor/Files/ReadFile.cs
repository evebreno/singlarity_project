﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameThor
{
    public class ReadFile : IReadFile
    {
        public byte[] Input { get { return actualStram.ToArray(); } set { actualStram = value.ToList(); } }
        public int ActualPosition { get; set; }

        private List<byte> actualStram;

        public ReadFile()
        {
            actualStram = new List<byte>();
        }
        public ReadFile(byte[] stream)
        {
            actualStram = stream.ToList();
        }

        public byte[] ReadUntil(byte stopByte)
        {
            List<byte> readBytes = new List<byte>();
            for(int i=ActualPosition; actualStram[ActualPosition] != stopByte && ActualPosition < actualStram.Count; ActualPosition++ )
            {
                readBytes.Add(actualStram[ActualPosition]);
            }

            return readBytes.ToArray();
        }
        public byte[] ReadUntil(byte stopByte, bool ignoreFirst)
        {
            List<byte> readBytes = new List<byte>();
            ActualPosition = ignoreFirst ? ActualPosition + 1 : ActualPosition;

            for (int i = ActualPosition; actualStram[ActualPosition] != stopByte && ActualPosition < actualStram.Count; ActualPosition++)
            {
                readBytes.Add(actualStram[ActualPosition]);
            }

            return readBytes.ToArray();
        }
        public byte[] ReadUntil(byte[] stopBytes, bool ignoreFirst)
        {
            List<byte> readBytes = new List<byte>();
            ActualPosition = ignoreFirst ? ActualPosition + 1 : ActualPosition;

            for (int i = ActualPosition; ActualPosition < actualStram.Count; ActualPosition++)
            {
                if (actualStram[ActualPosition] == stopBytes[0])
                {
                    if (CompareData(stopBytes, GetNext(ActualPosition, stopBytes.Length, false), true))
                    {
                        ActualPosition += stopBytes.Length - 1;
                        break;
                    }
                }
                readBytes.Add(actualStram[ActualPosition]);
            }

            return readBytes.ToArray();
        }

        public void JumpStandardHeader()
        {
            ReadUntil(255, true);
            ReadUntil(255, true);
        }

        /// <summary>
        /// Checa se o byte atual condiz com o esperado, mas não o consome
        /// </summary>
        /// <param name="byteToCheck">Byte a ser checado</param>
        /// <param name="consumeFirst">Consome o primeiro byte e ler o próximo.</param>
        /// <returns>True se confere</returns>
        public bool CheckFirst(byte byteToCheck, bool consumeFirst)
        {
            ActualPosition = consumeFirst ? ActualPosition + 1 : ActualPosition;
            return actualStram[ActualPosition] == byteToCheck;
        }

        /// <summary>
        /// Retorna os valores de uma parte do documento sem consumir os dados
        /// </summary>
        /// <param name="position">Posição inicial a ser lida</param>
        /// <param name="count">Quantidade de bytes a serem lidos</param>
        /// <param name="ignoreFirst">Pula o primeiro byte e lê o próximo</param>
        public byte[] GetNext(int position, int count, bool ignoreFirst)
        {
            int index = (ignoreFirst) ? position + 1 : position;
            List<byte> bytesRead = new List<byte>();

            for(int i = 0;i<count;i++)
            {
                bytesRead.Add(actualStram[index]);
                index++;
            }

            return bytesRead.ToArray();
        }

        /// <summary>
        /// Compara o valor de dois arrays de bytes
        /// </summary>
        /// <param name="model">Modelo a ser usado</param>
        /// <param name="productToVerify">Valor para ser verificado</param>
        /// <param name="LengthMatter">Indica se os arrays devem ter obrigatoriamente o mesmo tamanho ou não</param>
        public bool CompareData(byte[] model, byte[] productToVerify, bool LengthMatter)
        {
            bool result = true;
            if(LengthMatter)
            { if(model.Length != productToVerify.Length) { result = false; goto FINISH; } }

            for(int i=0;i<model.Length && i<productToVerify.Length;i++)
            {
                if(model[i] != productToVerify[i]) { result = false; goto FINISH; }
            }

            FINISH:;
            return result;
        }
    }
}
