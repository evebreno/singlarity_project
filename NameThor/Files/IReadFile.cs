﻿namespace NameThor
{
    public interface IReadFile
    {
        int ActualPosition { get; set; }
        byte[] Input { get; set; }

        byte[] ReadUntil(byte stopByte);
        byte[] ReadUntil(byte stopByte, bool ignoreFirst);
        byte[] ReadUntil(byte[] stopBytes, bool ignoreFirst);
        void JumpStandardHeader();
        bool CheckFirst(byte byteToCheck, bool consumeFirst);
    }
}