﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NameThor.Dicionarios;

namespace NameThor
{
    public class Engraver
    {
        public void Engrave(Dictionary<char, List<CharRule>> dataToSave)
        {
            MakeFile maker = new MakeFile();
            foreach(var regra in dataToSave)
            {
                maker.InsertByte(0);
                maker.InsertByte(StandardEncoder.GetByte(regra.Key));
                maker.InsertByte(1);
                foreach(var actRegra in regra.Value)
                {
                    maker.InsertByte(2);
                    maker.InsertByte(StandardEncoder.GetByte(actRegra.Before));
                    maker.InsertByte(255);
                    maker.InsertByte(3);
                    maker.InsertByte(StandardEncoder.GetByte(actRegra.After));
                    maker.InsertByte(255);
                }
                maker.InsertByte(255);
            }
            FileStream fs = new FileStream($"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}/database.ntwdf", FileMode.OpenOrCreate);
            fs.Write(maker.Output, 0, maker.Output.Length);
            fs.Close();
        }
    }
}
