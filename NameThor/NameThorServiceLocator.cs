﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameThor
{
    static public class NameThorServiceLocator
    {
        static public IName SearchNameGenerator()
        {
            return new NameCreator();
        }

        static public void CreateNames()
        {
            NameInterpreter.InterpretWords();
        }
    }
}
