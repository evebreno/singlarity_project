﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameThor.Dicionarios;

namespace NameThor
{
    [Obsolete("Classe muito custosa à memória!")]
    class NameGenerator : IName
    {
        Random r = new Random();
        public string GenerateName(int WordNumber)
        {
            string valuableName = "";
            for (int i = 0; i < WordNumber; i++)
            {
                string[] Names = GetWordList(GetDic(r.Next(1, 4)));

                valuableName += (i != 0) ? " " : "";
                valuableName += $"{Names[r.Next(0, Names.Length - 1)]}";
            }

            return valuableName;
        }

        private string GetDic(int dicNumber)
        {
            switch (dicNumber)
            {
                case 1: return Dics.african;
                case 2: return Dics.finnish;
                case 3: return Dics.swahili;
                default: return Dics.finnish;
            }
        }

        private string[] GetWordList(string dictionaryToCheck)
        {
            string[] value = dictionaryToCheck.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < value.Length; i++)
            {
                value[i] = value[i].Trim('\n');
                value[i] = value[i].Trim('\r');
            }

            return value;
        }

    }
}
