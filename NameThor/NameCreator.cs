﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameThor
{
    [Obsolete("Use NameGenerator")]
    internal class NameCreator : IName
    {
        static Random r = new Random();
        public string GenerateName(int wordNumber)
        {   
            string result = "";

            byte[] buffer = new byte[256];
            r.NextBytes(buffer);

            for (int i = 0; i < wordNumber; i++)
            {
                result += (i != 0) ? " " : "";
                result += CreateSingleName();
            }

            return result;
        }

        private string CreateSingleName()
        {
            byte[] buffer = new byte[r.Next(1, 17)];
            List<char> name = new List<char>();
            r.NextBytes(buffer);
            string tmpName = "";

            foreach (char c in CreateUnfomalizedName()) { name.Add(c); }
            foreach (char c in name) { tmpName += c; }

            int forcedIndex = -1;
            int times = 0;
            while(!NameInterpreter.ValidateName(tmpName, out int errorIndex))
            {
                if (forcedIndex < errorIndex) forcedIndex = errorIndex;

                if (times >= 16)
                {
                    name.Clear();
                    foreach (char c in CreateUnfomalizedName()) { name.Add(c); }
                    times = 0;
                    forcedIndex = 0;
                } 

                name[forcedIndex] = NameInterpreter.KnownChars[r.Next(0, NameInterpreter.KnownChars.Length)];

                tmpName = "";
                foreach (char c in name) { tmpName += c; }
                times++;
            }

            return tmpName;
        }

        private string CreateUnfomalizedName()
        {
            List<char> name = new List<char>();
            for (int i = 0; i < r.Next(3, 32); i++)
            {
                name.Add(NameInterpreter.KnownChars[r.Next(0, NameInterpreter.KnownChars.Length)]);
            }

            var tmpName = "";
            foreach (char c in name) { tmpName += c; }

            return tmpName;
        }
    }
}
