﻿//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NameThor.Dicionarios {
    using System;
    
    
    /// <summary>
    ///   Uma classe de recurso de tipo de alta segurança, para pesquisar cadeias de caracteres localizadas etc.
    /// </summary>
    // Essa classe foi gerada automaticamente pela classe StronglyTypedResourceBuilder
    // através de uma ferramenta como ResGen ou Visual Studio.
    // Para adicionar ou remover um associado, edite o arquivo .ResX e execute ResGen novamente
    // com a opção /str, ou recrie o projeto do VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Dics {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Dics() {
        }
        
        /// <summary>
        ///   Retorna a instância de ResourceManager armazenada em cache usada por essa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NameThor.Dicionarios.Dics", typeof(Dics).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Substitui a propriedade CurrentUICulture do thread atual para todas as
        ///   pesquisas de recursos que usam essa classe de recurso de tipo de alta segurança.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Consulta uma cadeia de caracteres localizada semelhante a aaklig
        ///aaklige
        ///aakliger
        ///aaklighede
        ///aakligheid
        ///aakligs
        ///aakligste
        ///aal
        ///aalagtig
        ///aalagtige
        ///aalagtiges
        ///aalastafel
        ///aalbessie
        ///aalbessies
        ///aalglad
        ///aalgladde
        ///aalglade
        ///aalmoes
        ///aalmoese
        ///aalmoesenier
        ///aalmoeseniere
        ///aalmoeseniers
        ///aalstreep
        ///aaltjiesiekte
        ///aalvormig
        ///aalwagters
        ///aalwee
        ///aalweeagtig
        ///aalweeagtige
        ///aalweeagtigste
        ///aalweebitter
        ///aalweeboom
        ///aalweehars
        ///aalweehennep
        ///aalweepil
        ///aalwees
        ///aalweesap
        ///aalwurm
        ///aalwurms
        ///aalwyn
        ///aalwynbitter
        ///aalwynblaar
        ///aalwynblare
        ///aalwynboom
        ///aalwyne
        ///aalw [o restante da cadeia de caracteres foi truncado]&quot;;.
        /// </summary>
        internal static string african {
            get {
                return ResourceManager.GetString("african", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Byte[].
        /// </summary>
        internal static byte[] database {
            get {
                object obj = ResourceManager.GetObject("database", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Consulta uma cadeia de caracteres localizada semelhante a aabraham
        ///aabrahamilla
        ///aabrahamin
        ///aabrahamista
        ///aadolf
        ///aadolfin
        ///aake
        ///aakkoset
        ///aakkoshakemiston
        ///aakkosia
        ///aakkosilla
        ///aakkosjärjestykseen
        ///aakkosjärjestyksessä
        ///aakkostoa
        ///aale
        ///aalloilla
        ///aalloissa
        ///aallokko
        ///aallokkokoskessa
        ///aallokon
        ///aallokossa
        ///aalloksi
        ///aallolle
        ///aallolta
        ///aallon
        ///aallonharja
        ///aallonharjalla
        ///aallonharjalle
        ///aallonharjan
        ///aallonpituudella
        ///aallonpituuksilla
        ///aallonpituus
        ///aallonpituusalueet
        ///aallonpituusalueilla
        ///aallonpohja
        ///aallonpohjassa
        ///aallonpohjasta
        ///aallossa
        ///aallosta
        ///a [o restante da cadeia de caracteres foi truncado]&quot;;.
        /// </summary>
        internal static string finnish {
            get {
                return ResourceManager.GetString("finnish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Consulta uma cadeia de caracteres localizada semelhante a aliopewa
        ///aba
        ///abadoni
        ///abarikiwe
        ///abeli
        ///abia
        ///abiathari
        ///abilene
        ///abiudi
        ///abiya
        ///abrahamu
        ///acha
        ///achaneni
        ///acheni
        ///adamu
        ///adhabu
        ///adi
        ///adiramito
        ///admini
        ///adria
        ///adui
        ///afadhali
        ///afia
        ///agabo
        ///agano
        ///agripa
        ///aha
        ///ahaa
        ///ahadi
        ///ahazi
        ///ahidi
        ///aibu
        ///ainoni
        ///akaamuru
        ///akaanguka
        ///akaanza
        ///akaaye
        ///akaenda
        ///akaendelea
        ///akafanya
        ///akafika
        ///akafundisha
        ///akafunga
        ///akaiko
        ///akailazimisha
        ///akaingia
        ///akaitwaa
        ///akaja
        ///akajificha
        ///akajitupa
        ///akakaa
        ///akaketi
        ///akakichukua
        ///akaligusa
        ///akalikamata
        ///akalikodisha
        ///akaliwa
        ///akalizun [o restante da cadeia de caracteres foi truncado]&quot;;.
        /// </summary>
        internal static string swahili {
            get {
                return ResourceManager.GetString("swahili", resourceCulture);
            }
        }
    }
}
