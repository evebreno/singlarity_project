﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameThor.Dicionarios;

namespace NameThor
{
    internal static class NameInterpreter
    {
        static public Dictionary<char, List<CharRule>> Rules { get { if (!alreadyRead) { GetDictionaryRules(); } return _rules; } }
        static public char[] KnownChars
        {
            get
            {
                char[] caracs = new char[Rules.Count];

                int index = 0;
                foreach(var rule in Rules)
                {
                    caracs[index] = rule.Key;
                    index++;
                }

                return caracs;
            }
        }

        static private Dictionary<char, List<CharRule>> _rules = new Dictionary<char, List<CharRule>>();
        static private bool alreadyRead = false;

        //[Obsolete("Uso apenas durante o desenvolvimento", true)]
        static public void InterpretWords()
        {
            NameGenerator gen = new NameGenerator();

            for (int i = 0; i < 1500; i++)
            {
                string actName = gen.GenerateName(1);
                ParseDocument(actName);
                int value = 0;
                foreach (var rule in Rules) { value += rule.Value.Count; }
                Console.BackgroundColor = ConsoleColor.DarkBlue; Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"\t\t\tNOME USADO: {actName}"); Console.ResetColor();
                Console.WriteLine($"Regras adicionadas; total de caracteres: {Rules.Count}, Total de regras: {value}\t\t\t\t.{i}");
            }

            Engraver cova = new Engraver();
            cova.Engrave(Rules);
        }

        static private void ParseDocument(string doc)
        {
            for (int i = 0; i < doc.Length; i++)
            {
                char bchar, achar;
                try { bchar = doc[i - 1]; }
                catch { bchar = new char(); }
                try { achar = doc[i + 1]; }
                catch { achar = new char(); }

                try
                {
                    foreach (CharRule regra in Rules[doc[i]])
                    {
                        if (regra.Before == bchar && regra.After == achar) { goto NEXTIT; }
                    }
                }
                catch { _rules.Add(doc[i], new List<CharRule>()); }
                _rules[doc[i]].Add(new CharRule(bchar, achar));
        
                NEXTIT:;
            }
        }

        static public bool ValidateName(string nameToValidade, out int errorIndex)
        {
            errorIndex = -1;
            for (int i = 0; i < nameToValidade.Length; i++)
            {
                char bchar, achar;
                try { bchar = nameToValidade[i - 1]; } catch { bchar = new char(); }
                try { achar = nameToValidade[i + 1]; } catch { achar = new char(); }

                if(!ValidadeChar(nameToValidade[i], new CharRule(bchar, achar)))
                {
                    errorIndex = i;
                    return false;
                }
            }

            return true;
        }

        static private bool ValidadeChar(char charToValidade, CharRule RuleToValidade)
        {
            foreach(var rule in Rules[charToValidade])
            {
                if (rule.Before == RuleToValidade.Before && rule.After == RuleToValidade.After) return true;
            }
            return false;
        }

        static public void TestRules()
        {
            
        }

        static private void GetDictionaryRules()
        {
            _rules = new Dictionary<char, List<CharRule>>();
            ReadFile reader = new ReadFile(Dics.database);
            while(reader.ActualPosition < reader.Input.Length-1)
            {
                if  (reader.ActualPosition == 0 || reader.CheckFirst(0, true))
                {
                    reader.ActualPosition++;

                    char key = StandardEncoder.GetChar(reader.ReadUntil(1)[0]);
                    _rules.Add(key, new List<CharRule>());

                    while (reader.CheckFirst(2, true))
                    {
                        reader.ActualPosition++;
                        char bchar = StandardEncoder.GetChar(reader.ReadUntil(255)[0]);
                        char achar;

                        if (!reader.CheckFirst(3, true)) { throw new Exception(); }
                        achar = StandardEncoder.GetChar(reader.ReadUntil(255, true)[0]);
                        _rules[key].Add(new CharRule(bchar, achar));
                    }
                }
            }
            alreadyRead = true;
        }
    }

    public struct CharRule
    {
        public char Before { get; set; }
        public char After { get; set; }

        public CharRule(char before, char after)
        {
            Before = before;
            After = after;
        }
    }
}
