﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace BattleField.Coloring
{
    public class WPFColorConverter
    {
        static public Color ConvertConsoleColor(ConsoleColor colorToConvert)
        {
            return KernelConsoleConverter(colorToConvert);
        }

        static private Color KernelConsoleConverter(ConsoleColor colorToConvert)
        {
            switch (colorToConvert)
            {
                case ConsoleColor.Black: return Color.FromRgb(0, 0, 0);
                case ConsoleColor.DarkBlue: return Color.FromRgb(0, 0, 128);
                case ConsoleColor.DarkGreen: return Color.FromRgb(0, 128, 0);
                case ConsoleColor.DarkCyan: return Color.FromRgb(0, 128, 128);
                case ConsoleColor.DarkRed: return Color.FromRgb(128, 0, 0);
                case ConsoleColor.DarkMagenta: return Color.FromRgb(128, 0, 128);
                case ConsoleColor.DarkYellow: return Color.FromRgb(128, 128, 0);
                case ConsoleColor.Gray: return Color.FromRgb(192, 192, 192);
                case ConsoleColor.DarkGray: return Color.FromRgb(128, 128, 128);
                case ConsoleColor.Blue: return Color.FromRgb(0, 0, 255);
                case ConsoleColor.Green: return Color.FromRgb(0, 255, 0);
                case ConsoleColor.Cyan: return Color.FromRgb(0, 255, 255);
                case ConsoleColor.Red:  return Color.FromRgb(255, 0, 0);
                case ConsoleColor.Magenta: return Color.FromRgb(255, 0, 255);
                case ConsoleColor.Yellow: return Color.FromRgb(255, 255, 0);
                case ConsoleColor.White: return Color.FromRgb(255, 255, 255);
                default: return Color.FromRgb(0, 0, 0);
            }
        }
    }
}
