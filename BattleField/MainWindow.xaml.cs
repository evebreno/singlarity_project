﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BattleField.Coloring;

namespace BattleField
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Focus();
            PlayerConsoleBlock.Focus();
        }

        public void SetPlayerColorSchema(ConsoleColor fore, ConsoleColor back)
        {
            SolidColorBrush foreColor = new SolidColorBrush(WPFColorConverter.ConvertConsoleColor(fore));
            SolidColorBrush backColor = new SolidColorBrush(WPFColorConverter.ConvertConsoleColor(back));

            PlayerStoryBox.Foreground = foreColor;
            PlayerStoryBox.Background = backColor;

            PlayerConsoleBlock.Foreground = foreColor;
            PlayerConsoleBlock.Background = backColor;
        }

        public void SetEnemyColorSchema(ConsoleColor fore, ConsoleColor back)
        {
            SolidColorBrush foreColor = new SolidColorBrush(WPFColorConverter.ConvertConsoleColor(fore));
            SolidColorBrush backColor = new SolidColorBrush(WPFColorConverter.ConvertConsoleColor(back));

            EnemyStoryBlock.Foreground = foreColor;
            EnemyStoryBlock.Background = backColor;
        }

        public void WriteAtPlayerStory(string textToWrite)
        {
            PlayerStoryBox.AppendText($"{textToWrite}");
        }

        public void WriteAtEnemyStory(string textToWrite)
        {
            EnemyStoryBlock.AppendText($"{textToWrite}");
        }
    }
}
