﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BattleField
{
    public partial class MainWindow : Window
    {
        private void PlayerConsoleBlock_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PlayerConsoleBlock.Text.Contains("\n"))
            {
                ConsoleDataSentEventArgs consE = new ConsoleDataSentEventArgs()
                { CommandSent = PlayerConsoleBlock.Text.Trim('\n') };

                ConsoleBlockSentDataEvent.Invoke(this, consE);
                PlayerConsoleBlock.Clear();
            }
        }

        private void PlayerStoryBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            PlayerStoryBox.ScrollToVerticalOffset(PlayerStoryBox.ViewportHeight);
        }

        private void EnemyStoryBlock_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnemyStoryBlock.ScrollToVerticalOffset(EnemyStoryBlock.ViewportHeight);
        }

        public event EventHandler ConsoleBlockSentDataEvent;
    }

    public class ConsoleDataSentEventArgs : EventArgs
    {
        public string CommandSent { get; set; }

        public ConsoleDataSentEventArgs()
        {
            CommandSent = "";
        }

        public ConsoleDataSentEventArgs(string command)
        {
            CommandSent = command;
        }
    }
}
