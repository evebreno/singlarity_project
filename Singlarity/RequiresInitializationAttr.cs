﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class RequiresInitializationAttribute : Attribute
    {
        readonly string metodName;

        public RequiresInitializationAttribute(string initializationMethodName)
        {
            this.metodName = initializationMethodName;
            InitializationTime = InitializationPosition.Initialization;
            InitializationOrder = 10;
        }

        public string MethodName
        {
            get { return metodName; }
        }

        /// <summary>
        /// Quando o método será inicializado. (padrão: <see cref="InitializationPosition.Initialization"/>)
        /// </summary>
        public InitializationPosition InitializationTime { get; set; }
        /// <summary>
        /// Camada de prioridade. Padrão é 10.
        /// </summary>
        public byte InitializationOrder { get; set; }
        public enum InitializationPosition { Initialization = 0, PostInitialization = 1 }
    }
}
