﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.ChipEngine;

namespace Singlarity
{
    public class Chip
    {
        public string Nome { get; set; }
        public Sigma Signo { get; set; }
        public IChakter[] Carac { get; set; }
        public ChipColor Cor { get; set; }

        public Chip()
        {
            Nome = null;
            Signo = new Sigma();
            Carac = new IChakter[0];
            Cor = ChipColorData.Esquemas[0];
        }

        public Chip(string nome,Sigma signo, IChakter[] carac, ChipColor cor)
        {
            Nome = nome;
            Signo = signo;
            Carac = carac;
            Cor = cor;
        }
    }
}
