﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Store
{
    static public class StoreConversionUtility
    {
        [Obsolete]
        static public byte ConvertToSepa(char num)
        {
            if (num.ToString().ToLower() == "a") return 40;
            else if (num.ToString().ToLower() == "b") return 63;
            else return 82;
        }
    }
}
