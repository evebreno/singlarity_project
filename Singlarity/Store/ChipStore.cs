﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Singlarity.ChipEngine;
using Singlarity.Store.Formularios;
using System.Windows.Forms;
using System.Diagnostics;
using Singlarity.User;

namespace Singlarity.Store
{
    [RequiresInitialization("SetPresentation", InitializationOrder = 11)]
    public class ChipStore
    {
        static public void SetPresentation()
        {
            Loja.GatheringFunctions += Loja_GatheringFunctions;
        }
        private static void Loja_GatheringFunctions(object sender, EventArgs e)
        {
            Loja.AddFunction(new FunctionToMainBoard(LojaCommands_PTBR.BuyChip_f, LojaCommands_PTBR.BuyChip_d));

            Loja.CallingFunction += Loja_CallingFunction;
        }

        private static void Loja_CallingFunction(string especificFunction)
        {
            if(especificFunction == LojaCommands_PTBR.BuyChip_f)
            {
                BuyChip();
            }
        }

        private static void BuyChip()
        {
            Log.ShowLine($"O preço do chip hoje é: ${DailyBalancement.ChipValue},00 glty");
            Log.ShowLine("Digite sim para comprar um chip novo.");
            string comand = Console.ReadLine().ToLower();
            Chip tmpChip;

            if (comand == "sim")
            {
                if (ValidadeShop(/*DailyBalancement.ChipValue*/float.MinValue, true))
                {
                    tmpChip = ConstructChip(out bool sucesso);
                    if(!sucesso) { Log.ShowWarningLine("Compra cancelada!"); goto Alt; }
                    Log.ShowWarningLine($"O preço da modulação é: {CalculateModPrice(tmpChip.Carac[0].Sepa)}");
                    Log.ShowWarningLine($"O preço total a ser pago é: {CalculateModPrice(tmpChip.Carac[0].Sepa)+DailyBalancement.ChipValue}");
                    Log.ShowWarning($"Digite sim para comprar: ");

                    comand = Console.ReadLine().ToLower();
                    if(comand == "sim" && ValidadeShop(CalculateModPrice(tmpChip.Carac[0].Sepa) + DailyBalancement.ChipValue, true))
                    {
                        Protagonista.RemoveCoin(CalculateModPrice(tmpChip.Carac[0].Sepa) + DailyBalancement.ChipValue);
                        Protagonista.AddChip(tmpChip);
                    }
                }
            }
            Alt:;
        }

        private static Chip ConstructChip(out bool sucesso)
        {
            Chip chip;
            sucesso = StoreWorkshop.StartChipConstructionProcess(out chip);

            return chip;
        }
        private static float CalculateModPrice(byte[] modulation)
        {
            float value = 0;
            foreach(byte current in modulation)
            {
                foreach(var sepaVerify in DailyBalancement.Sepas)
                {
                    if(current == sepaVerify.Referencia) { value += sepaVerify.Preço; break; }
                }
            }

            return value;
        }

        /// <summary>
        /// Verifica se o usuário pode comprar ou não
        /// </summary>
        /// <param name="valueToValidade">Valor do produto</param>
        static public bool ValidadeShop(float valueToValidade)
        {
            float maxCredit = -DailyBalancement.StandardMargin * Protagonista.Credit;
            float tempValue = Protagonista.Coin - valueToValidade;

            return tempValue >= maxCredit;
        }

        /// <summary>
        /// Verifica se o usuário pode comprar ou não e mostra um aviso ao console caso não possa
        /// </summary>
        /// <param name="valueToValidade">Valor do produto</param>
        /// <param name="showlog">Mostrar aviso ao console</param>
        static public bool ValidadeShop(float valueToValidade, bool showlog)
        {
            float maxCredit = -DailyBalancement.StandardMargin * Protagonista.Credit;
            float tempValue = Protagonista.Coin - valueToValidade;
            if(tempValue < maxCredit)
            {
                Log.ShowErrorLine("Desculpe, a compra não pôde ser realizada!");
                Log.ShowErrorLine($"Previsão de saldo: ${tempValue} glty\nCredito liberado: ${-maxCredit} glty");
            }

            return tempValue >= maxCredit;
        }
    }
}