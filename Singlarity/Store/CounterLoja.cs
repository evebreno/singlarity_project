﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Store
{
    [RequiresInitialization("SetPresentation")]
    [RequiresInitialization("Initialize", InitializationOrder = 12)]
    static public partial class Loja
    {
        static public event EventHandler GatheringFunctions;
        static public event LojaCallingFunctionEventHandler CallingFunction;
        static public List<MainBoardFunctions> Functions { get { return functions; } private set { functions = value; } }
        static private List<MainBoardFunctions> functions;

        static public void Initialize()
        {
            functions = new List<MainBoardFunctions>(); //TODO: Ainda com o sistema antigo de busca de funções

            StartGathering();
        }

        static private void StartGathering()
        {
            GatheringFunctions.Invoke(null, new EventArgs());
        }

        static public void AddFunction(MainBoardFunctions function)
        {
            bool canAdd = true;
            foreach (MainBoardFunctions mbf in functions)
            {
                canAdd = (mbf.Function == function.Function) ? false : true;
                if (!canAdd)
                {
                    throw new Exception("Ocorreu um conflito no momento de adição de funções: " + function.Function);
                }
            }

            if (canAdd)
            {
                functions.Add(function);
            }
        }
    }

    public delegate void LojaCallingFunctionEventHandler(string especificFunction);
}
