﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Singlarity.ChipEngine;
using Singlarity.SepaEngine;
using Singlarity.Store.Formularios;
using System.Windows.Forms;

namespace Singlarity.Store
{
    static public class StoreWorkshop
    {
        /// <summary>
        /// Inicia o processo de criação de chip assistido pelo usuário
        /// </summary>
        /// <returns><see cref="true"/> se operação bem-sucedida</returns>
        static public bool StartChipConstructionProcess(out Chip chip)
        {
            string nome;
            Sigma signo;
            ChakterTipo chakter;
            ChipColor Schema;
            string chakNom, chakDef;
            byte[] chakSepa;
            chip = null;

            Log.ShowWarningLine("DIGITE SAIR A QUALQUER MOMENTO PARA CANCELAR O PROCESSO!");

            Log.ShowWarning("Digite o nome do chip: ");
            if (CheckSair(Console.ReadLine(), out nome)) { return false; }
            signo = SelectSigma();
            Schema = SelectTheme();

            Log.ShowWarning("Digite o nome do chak: ");
            if (CheckSair(Console.ReadLine(), out chakNom)) { return false; }

            Log.ShowWarning("Defina o chak (opcional) [ENTER para deixar em branco]: ");
            if (CheckSair(Console.ReadLine(), out chakDef)) { return false; }
            chakter = SelectChakter();
            chakSepa = DescribeSepaModulation(chakter);

            if(nome.Trim(' ') == "" || chakNom.TrimStart(' ') == "") { Log.ShowErrorLine("Algum dos nomes é inválido!");  return false; }
            IChakter[] carac = { ServiceLocator<IChakter>.SearchClass(null, new object[] { chakNom, chakDef, chakSepa,  (float)1, chakter }) };
            chip = new Chip(nome, signo, carac, Schema);

            return true;
        }

        static private Sigma SelectSigma()
        {
            //Os signos do chip serão para definir quais conjuntos de funções em batalha cada chip pode realizar
            //os signos das características definem os comportamento das entradas de ambiente
            bool sucess = false;
            Sigma result = new Sigma();

            while (!sucess)
            {
                Log.ShowLine("Escolha um dos signos (digite o número correspondente ao signo) [signo do chip]: ");
                Log.ShowLine($"0-{Sigma.selvitystilauhan}\n1-{Sigma.meskaliinissa}\n2-{Sigma.kommodoor}\n3-{Sigma.unyonge}\n4-{Sigma.walijiunga}");

                int number;
                string tmpString;
                if (CheckSair(Console.ReadLine(), out tmpString)) { throw new ActionCancelledByUserException("Ação cancelada com sucesso!"); }
                else
                {
                    if (!int.TryParse(tmpString, out number)) Log.ShowErrorLine("O caractere digitado não corresponde a um numero! Tente novamente...");
                    else if (number > 4 || number < 0) { Log.ShowErrorLine("O número digitado não corresponde a nenhum signo! Tente novamente..."); continue; }
                    else { result = (Sigma)number; sucess = true; }
                }
            }

            return result;
        }

        static private ChipColor SelectTheme()
        {
            bool sucess = false;
            ChipColor result = new ChipColor(new ConsoleColor(), new ConsoleColor(), "");
            while(!sucess)
            {
                Log.ShowWarningLine("Escolha o tema: (Digite o número correspondente ao tema desejado)");
                for (int i = 0; i < ChipColorData.Esquemas.Length; i++)
                {
                    Console.BackgroundColor = ChipColorData.Esquemas[i].BackColor;
                    Console.ForegroundColor = ChipColorData.Esquemas[i].ForeColor;
                    Console.WriteLine($"{i}-{ChipColorData.Esquemas[i].Name}");
                }
                Console.ResetColor();

                string tmpString;
                int number;
                if (CheckSair(Console.ReadLine(), out tmpString)) { throw new ActionCancelledByUserException("Ação cancelada com sucesso!"); }
                else
                {
                    if (!int.TryParse(tmpString, out number)) Log.ShowErrorLine("O caractere digitado não corresponde a um numero! Tente novamente...");
                    else if (number < 0 || number > ChipColorData.Esquemas.Length - 1) { Log.ShowErrorLine("O número digitado não corresponde a nenhum esquema! Tente novamente..."); continue; }
                    else { result = ChipColorData.Esquemas[number]; sucess = true; }
                }
            }

            return result;
        }

        static private ChakterTipo SelectChakter()
        {
            bool sucess = false;
            ChakterTipo result = new ChakterTipo();

            while (!sucess)
            {
                Log.ShowLine("Escolha um dos signos (digite o número correspondente ao signo)[signo do chak]: ");
                Log.ShowLine($"0-{ChakterTipo.versbundel}\n1-{ChakterTipo.wafungwa}\n2-{ChakterTipo.jäätelööä}\n3-{ChakterTipo.teksteillään}\n4-{ChakterTipo.nepalista}\n5-{ChakterTipo.inlewe}");

                int number;
                string tmpString;
                if (CheckSair(Console.ReadLine(), out tmpString)) { throw new ActionCancelledByUserException("Ação cancelada com sucesso!"); }
                else
                {
                    if (!int.TryParse(tmpString, out number)) Log.ShowErrorLine("O caractere digitado não corresponde a um numero! Tente novamente...");
                    else if (number > 5 || number < 0) { Log.ShowErrorLine("O número digitado não corresponde a nenhum signo! Tente novamente..."); continue; }
                    else
                    {
                        result = (ChakterTipo)Math.Pow(2, number);
                        sucess = true;
                    }
                }
            }

            return result;
        }
        
        static private byte[] DescribeSepaModulation(ChakterTipo afinidade)
        {
            Thread tarefa = new Thread(CallSepaTable);
            tarefa.IsBackground = true;
            tarefa.Start();

            Log.ShowLine("Descreva a modulação (digite sair para finalizar a modulação)");
            List<byte> modulation = new List<byte>();
            string command = "";
            while(!CheckSair(Console.ReadLine(), out command))
            {
                byte tmpValue;
                if(byte.TryParse(command, out tmpValue))
                {
                    if(SepaCatalog.CheckExistence(tmpValue, afinidade))
                    {
                        modulation.Add(tmpValue);
                    }
                }

                else //Se não encotrar o valor numérico, o sistema busca a referencia literal do apoentador
                {
                    bool found = false;
                    foreach(ITemporarySepaStoreInformation sepaInfo in DailyBalancement.Sepas)
                    {
                        if(sepaInfo.Apontador == command.ToLower())
                        {
                            modulation.Add(sepaInfo.Referencia);
                            found = true;
                            break;
                        }
                    }

                    if(!found) { Log.ShowErrorLine("Valor não encontrado, tente novamente..."); }
                }
            }

            int errorIndex = 0;
            if(SepaCatalog.CheckModuleConsistence(modulation.ToArray(), afinidade, out errorIndex))
            {
                Log.ShowLine("Modulo ok!");
                return modulation.ToArray();
            }

            else { Log.ShowErrorLine($"Módulo inconsistente: problema no índice {errorIndex} do módulo"); return new byte[0]; }
        }

        static private void CallSepaTable()
        {
            Application.Run(new AvaliableSepaForm(DailyBalancement.Sepas));
        }

        /// <summary>
        /// Verifica se o comando digitado corresponde a "sair".
        /// </summary>
        /// <param name="comand">comando para ser verificado</param>
        /// <param name="buffer"><see cref="string"/> a ser armazenada caso não se trate do comando "sair"</param>
        /// <returns><see cref="true"/> se for o comando "sair"</returns>
        /// <remarks>Se for o comando sair, buffer receberá uma string vazia</remarks>
        static private bool CheckSair(string comand, out string buffer)
        {
            if(comand.ToLower() == "sair")
            {
                buffer = "";
                return true;
            }
            else { buffer = comand; return false; }
        }
    }

    public class ActionCancelledByUserException : Exception
    {
        public override string Message => _message;
        private string _message;

        public ActionCancelledByUserException()
        {
            _message = base.Message;
        }
        public ActionCancelledByUserException(string mensagem)
        {
            _message = mensagem;
        }
    }
}
