﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Store
{
    [RequiresInitialization("SetPresentation", InitializationOrder = 11)]
    sealed class LojaUtility
    {
        static public void SetPresentation()
        {
            Loja.GatheringFunctions += Loja_GatheringFunctions;
        }

        private static void Loja_GatheringFunctions(object sender, EventArgs e)
        {
            Loja.AddFunction(new FunctionToMainBoard(Commands_PTBR.Commands_f, Commands_PTBR.Commands_d));
            Loja.AddFunction(new FunctionToMainBoard(Commands_PTBR.Cls_f, Commands_PTBR.Cls_d));
            Loja.AddFunction(new FunctionToMainBoard(Commands_PTBR.Exit_f, Commands_PTBR.Exit_d));

            Loja.CallingFunction += Loja_CallingFunction;
        }

        private static void Loja_CallingFunction(string especificFunction)
        {
            if (especificFunction == Commands_PTBR.Commands_f || especificFunction == "help" || especificFunction == "ls")
            {
                foreach (MainBoardFunctions mbf in Loja.Functions) Log.ShowLine($"{mbf.Function}\t\t{mbf.Definition}");
            }

            else if (especificFunction == Commands_PTBR.Cls_f || especificFunction == "cls") Console.Clear();
            else if (especificFunction == Commands_PTBR.Exit_f || especificFunction == "exit") Loja.inLoja = false;
        }
    }
}
