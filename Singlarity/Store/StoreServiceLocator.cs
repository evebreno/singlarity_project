﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.Store.Formularios;

namespace Singlarity.Store
{
    [Obsolete("Use ServiceLocator")]
    public static class StoreServiceLocator
    {
        static public ITemporarySepaStoreInformation SearchTemporarySepaStoreInformationClass(string cod, byte refer, string pointer, float price)
        {
            return new TemporarySepaStoreInformation(cod, refer, pointer, price);
        }
    }
}
