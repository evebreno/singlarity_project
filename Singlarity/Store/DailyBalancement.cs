﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.Store.Formularios;
using Singlarity.SepaEngine;

namespace Singlarity.Store
{
    static public class DailyBalancement
    {
        static private ITemporarySepaStoreInformation[] _sepaStoreInformation;
        static private bool _sepaInformationInitialized = false;
        static private int _apontadorIndex = -1;

        static public float ChipValue
        {
            get
            {
                int seed = DateTime.Today.DayOfYear;
                seed += Environment.CurrentManagedThreadId;
                Random r = new Random(seed);

                return r.Next(650, 20000);
            }
        }
        static public int StandardMargin
        {
            get
            {
                int seed = DateTime.Now.Hour;
                Random r = new Random(seed);

                return r.Next(0, 15999);
            }
        }
        static public ITemporarySepaStoreInformation[] Sepas
        {
            get
            {
                InitializeSepaInformation();
                return _sepaStoreInformation;
            }
        }        


        static private string GetApontador()
        {
            _apontadorIndex++;
            switch (_apontadorIndex)
            {
                case 0: return "a";
                case 1: return "b";
                case 2: return "c";
                case 3: return "d";
                case 4: return "e";
                case 5: return "f";
                case 6: return "g";
                case 7: return "h";
                case 8: return "i";
                case 9: return "j";
                case 10: return "k";
                case 11: return "l";
                case 12: return "m";
                case 13: return "n";
                case 14: return "o";
                case 15: return "p";
                case 16: return "q";
                case 17: return "r";
                case 18: return "s";
                case 19: return "t";
                case 20: return "u";
                case 21: return "v";
                case 22: return "w";
                case 23: return "x";
                case 24: return "y";
                case 25: return "z";
                default: throw new Exception();
            }
        }               
        static private void InitializeSepaInformation()
        {
            if(!_sepaInformationInitialized)
            {
                int seed = DateTime.Today.DayOfYear;
                int size;
                seed += Environment.CurrentManagedThreadId;
#if DEBUG
                Random r = new Random();
#else
                Random r = new Random(seed);
#endif

                size = r.Next(0, 256);
                byte[] tmpBuffer = new byte[size];
                r.NextBytes(tmpBuffer);

                List<byte> b = new List<byte>();
                List<ITemporarySepaStoreInformation> storeInformation = new List<ITemporarySepaStoreInformation>();
                foreach (byte tmpb in tmpBuffer)
                {
                    if (SepaCatalog.CheckExistence(tmpb) && !b.Contains(tmpb)) b.Add(tmpb);
                }

                int normal = 69;
                foreach (byte tmpb in b)
                {
                    string pointer;
                    try { pointer = GetApontador(); }
                    catch { pointer = ""; }

                    r = new Random(seed + normal);
                    storeInformation.Add(ServiceLocator<ITemporarySepaStoreInformation>.SearchClass(null, new object[] { tmpb.ToString(), tmpb, pointer, (float)r.Next(10, 2500) }));

                    normal++;
                }

                _sepaInformationInitialized = true;
                _sepaStoreInformation = storeInformation.ToArray();
            }
        }
    }
}