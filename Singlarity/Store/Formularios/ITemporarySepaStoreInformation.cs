﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Store.Formularios
{
    public interface ITemporarySepaStoreInformation
    {
        string Cod { get; }
        byte Referencia { get; }
        string Apontador { get; }
        float Preço { get; }
    }
}
