﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Singlarity.Store.Formularios
{
    public partial class AvaliableSepaForm : Form
    {
        public AvaliableSepaForm(ITemporarySepaStoreInformation[] internalInformation)
        {
            InitializeComponent();
            AddInfo(internalInformation);
        }

        private void AddInfo(ITemporarySepaStoreInformation[] internalInformation)
        {
            foreach (ITemporarySepaStoreInformation inf in internalInformation)
            {
                iTemporarySepaStoreInformationBindingSource.Add(inf);
            }
        }
    }
}
