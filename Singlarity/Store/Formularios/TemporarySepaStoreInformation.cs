﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Store.Formularios
{
    [Localizable(typeof(ITemporarySepaStoreInformation))]
    public class TemporarySepaStoreInformation : ITemporarySepaStoreInformation
    {
        public string Cod => _cod;
        public byte Referencia => _ref;
        public string Apontador => _apontador;
        public float Preço => _preco;

        private string _cod;
        private byte _ref;
        private string _apontador;
        private float _preco;

        public TemporarySepaStoreInformation(string cod, byte refer, string pointer, float price)
        {
            _cod = cod;
            _ref = refer;
            _apontador = pointer;
            _preco = price;
        }
    }
}