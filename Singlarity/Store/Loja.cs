﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.User;

namespace Singlarity.Store
{
    static public partial class Loja
    {
        static public void SetPresentation()
        {
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Store_f, Commands_PTBR.Store_d, new MainBoardFunctionDelegation(typeof(Loja), "StartBoard") }));
        }

        static public bool inLoja = false;
        public static void StartBoard()
        {
            if (Protagonista.HasUser)
            {
                inLoja = true;

                while (inLoja)
                {
                    Log.Show($"{Protagonista.Nome}[Loja]->");

                    string comand = Console.ReadLine().ToLower();
                    Console.WriteLine();
                    CallingFunction.Invoke(comand);
                    Console.WriteLine();
                }
            }

            else
            {
                Log.ShowWarningLine("Desculpe, é necessário, primeiramente definir um usuário!");
            }
        }
    }
}
