﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Singlarity
{
    static public class ServiceLocator<T>
    {
        static LocalizableReference[] LocalizableClasses
        {
            get
            {
                if (!foundClasses) FindClasses();
                return _localizableClass.ToArray();
            }
        }
        static List<LocalizableReference> _localizableClass = new List<LocalizableReference>();

        static private bool foundClasses = false;
        /// <summary>
        /// Cria uma instância de uma classe que atenda a interface solicitada
        /// </summary>
        /// <returns></returns>
        static public T SearchClass()
        {
            Type Result = typeof(T);
            T resp = default(T);
            foreach (var classe in LocalizableClasses)
            {
                foreach (var name in classe.MetaInfo.ForInterface)
                {
                    if (Result.FullName == name.FullName)
                    {
                        ConstructorInfo construct = classe.Tipo.GetConstructor(Type.EmptyTypes);
                        resp = (T)construct.Invoke(new object[] { });
                        goto FINISH;

                    }
                }
            }

            FINISH:;
            return resp;
        }
        /// <summary>
        /// Cria uma instância de uma classe que atenda a interface solicitada com a respectiva tag
        /// </summary>
        /// <param name="tag">tag a ser buscada na classe</param>
        static public T SearchClass(string tag)
        {
            Type Result = typeof(T);
            T resp = default(T);
            foreach (var classe in LocalizableClasses)
            {
                foreach (var name in classe.MetaInfo.ForInterface)
                {
                    if (Result.FullName == name.FullName && tag == classe.MetaInfo.Tag)
                    {
                        ConstructorInfo construct = classe.Tipo.GetConstructor(Type.EmptyTypes);
                        resp = (T)construct.Invoke(new object[] { });
                        goto FINISH;
                    }
                }
            }

            FINISH:;
            return resp;
        }
        /// <summary>
        /// Cria uma instância de uma classe que atenda a interface solicitada com os respectivos parâmetros e a respectiva tag
        /// </summary>
        /// <param name="args">argumentos a serem utilizados na classe</param>
        /// <param name="tag">tag a ser buscada</param>
        /// <returns></returns>
        static public T SearchClass(string tag, params object[] args )
        {
            Type Result = typeof(T);
            T resp = default(T);
            Type[] parametros = new Type[args.Length];

            for (int i = 0; i < args.Length; i++) { parametros[i] = args[i].GetType(); }

            foreach (var classe in LocalizableClasses)
            {
                foreach (var name in classe.MetaInfo.ForInterface)
                {
                    if (Result.FullName == name.FullName && classe.MetaInfo.Tag == tag)
                    {
                        ConstructorInfo construct = classe.Tipo.GetConstructor(parametros);
                        if (construct != null)
                        {
                            resp = (T)construct.Invoke(args);
                            goto FINISH;
                        }
                    }
                }
            }

            FINISH:;
            return resp;
        }

        static private void FindClasses()
        {
            Assembly a = Assembly.GetEntryAssembly();
            Type[] tipos = a.GetTypes();

            foreach (var tipo in tipos)
            {
                try
                {
                    LocalizableAttribute attr = tipo.GetCustomAttribute<LocalizableAttribute>();
                    if (attr != null)
                    { _localizableClass.Add(new LocalizableReference(tipo, attr)); }
                }
                catch { }
            }

            foundClasses = true;
        }
    }
}
