﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public class LocalizableReference
    {
        public Type Tipo { get; set; }
        public LocalizableAttribute MetaInfo { get; set; }

        public LocalizableReference()
        {
            Tipo = null;
            MetaInfo = null;
        }

        public LocalizableReference(Type tipo, LocalizableAttribute attr)
        {
            Tipo = tipo;
            MetaInfo = attr;
        }
    }
}
