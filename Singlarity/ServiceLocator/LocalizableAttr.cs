﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    /// <summary>
    /// Torna a classe visível para o <see cref="ServiceLocator{T}"/>
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class LocalizableAttribute : Attribute
    {
        readonly Type[] forInterface;

        public LocalizableAttribute(params Type[] InterfaceDiscovered)
        {
            this.forInterface = InterfaceDiscovered;
        }

        public Type[] ForInterface
        {
            get { return forInterface; }
        }

        public string Tag { get; set; }
    }
}
