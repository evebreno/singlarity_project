﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.ChipEngine
{
    static public class ChipColorData
    {
        static public ChipColor[] Esquemas
        {
            get
            {
                ChipColor[] esquemas =
                {
                    new ChipColor(ConsoleColor.Black, ConsoleColor.White, "mwangalieni"),
                    new ChipColor((ConsoleColor)0, (ConsoleColor)10, "postilaatikon"),
                    new ChipColor((ConsoleColor)4, (ConsoleColor)15, "utapigana"),
                    new ChipColor((ConsoleColor)4, (ConsoleColor)13, "legtyd"),
                    new ChipColor((ConsoleColor)0, (ConsoleColor)12, "hakijatoweka"),
                    new ChipColor((ConsoleColor)11, (ConsoleColor)1, "bynes"),
                    new ChipColor((ConsoleColor)6, (ConsoleColor)14, "akawauliza"),
                    new ChipColor((ConsoleColor)2, (ConsoleColor)15 , "poolsee"),
                    new ChipColor((ConsoleColor)3, (ConsoleColor)0, "veroministerin"),
                    new ChipColor((ConsoleColor)5, (ConsoleColor)10, "vilahdellut"),
                    new ChipColor((ConsoleColor)6, (ConsoleColor)7, "unaowatawala"),
                    new ChipColor((ConsoleColor)7, (ConsoleColor)1, "legkaart"),
                    new ChipColor((ConsoleColor)8, (ConsoleColor)15, "veertjie"),
                    new ChipColor((ConsoleColor)9, (ConsoleColor)15, "yesema"),
                    new ChipColor((ConsoleColor)10, (ConsoleColor)0, "pastor"),
                    new ChipColor((ConsoleColor)12, (ConsoleColor)14, "galasie"),
                    new ChipColor((ConsoleColor)13, (ConsoleColor)1, "mashujaa"),
                    new ChipColor((ConsoleColor)14, (ConsoleColor)4, "mfungueni"),
                    new ChipColor((ConsoleColor)15, (ConsoleColor)5, "hol"),
                    new ChipColor((ConsoleColor)2, (ConsoleColor)0, "dipkraal"),
                };


                return esquemas;
            }
        }

        static public ChipColor GetSchemaByName(string nome)
        {
            ChipColor color = null;
            foreach(var cor in Esquemas)
            {
                if (cor.Name.ToLower() == nome.ToLower()) color = cor;
            }

            return color;
        }
    }

    public class ChipColor
    {
        public ConsoleColor BackColor { get; private set; }
        public ConsoleColor ForeColor { get; private set; }
        public string Name { get; private set; }

        public ChipColor(ConsoleColor backColor, ConsoleColor foreColor, string name)
        {
            BackColor = backColor;
            ForeColor = foreColor;
            Name = name;
        }
    }
}
