﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.BattleBoard
{
    [RequiresInitialization("SetPresentation", InitializationOrder = 11)]
    static public class MainBattleBoardUtility
    {
        static public void SetPresentation()
        {
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Commands_f, Commands_PTBR.Commands_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Listar") }));
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Cls_f, Commands_PTBR.Cls_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Limpar") }));
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Exit_f, Commands_PTBR.Exit_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Sair") }));
            #region Funções Complementares
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "help", Commands_PTBR.Commands_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Listar", true) }));
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "ls", Commands_PTBR.Commands_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Listar", true) }));
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "cls", Commands_PTBR.Cls_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Limpar", true) }));
            MainBattleBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "exit", Commands_PTBR.Exit_d, new MainBoardFunctionDelegation(typeof(MainBattleBoardUtility), "Sair", true) }));
            #endregion
        }

        static public void Listar()
        {
            foreach (var functions in MainBattleBoard.Functions)
            {
                if (!functions.Delegation.Hidden)
                {
                    Log.ShowLine($"{functions.Function}\t\t{functions.Definition}");
                }
            }
        }
        static public void Limpar()
        {
            Console.Clear();
        }
        static public void Sair()
        {
            MainBattleBoard.inBattleBoard = false;
        }
    }
}
