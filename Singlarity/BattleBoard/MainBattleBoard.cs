﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity;
using Singlarity.User;
using System.Reflection;

namespace Singlarity.BattleBoard
{
    [RequiresInitialization("SetPresentation")]
    static public class MainBattleBoard
    {
        static public List<IMainBoardFunction> Functions { get => functions; }
        static private List<IMainBoardFunction> functions = new List<IMainBoardFunction>();

        static public void SetPresentation()
        {
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, Commands_PTBR.BattleBoard_f, Commands_PTBR.BattleBoard_d, new MainBoardFunctionDelegation(typeof(MainBattleBoard), "EnterBattleBoard")));
        }
        static public void EnterBattleBoard()
        {
            if (Protagonista.HasUser) { inBattleBoard = true; ReadCommands(); }
            else { Log.ShowWarningLine("Desculpe, é necessário que haja um usuário definido!"); }
        }

        static public bool inBattleBoard = false;
        static private void ReadCommands()
        {
            while (inBattleBoard)
            {
                Log.Show($"{Protagonista.Nome}[Painel de Batalha]->");

                string comand = Console.ReadLine().ToLower();
                Console.WriteLine();
                CallCommand(comand);
                Console.WriteLine();
            }
        }

        static private void CallCommand(string cmd)
        {
            foreach (var func in Functions)
            {
                if (func.Function == cmd)
                {
                    Type tipo = func.Delegation.Classe;
                    MethodInfo met = tipo.GetMethod(func.Delegation.Metodo);

                    met.Invoke(tipo, func.Delegation.Arguments);
                }
            }
        }

        static public void AddFunction(IMainBoardFunction func)
        {
            foreach (var fun in Functions)
            {
                if (fun.Function == func.Function) throw new AmbiguosCommandException("Comando já inserido!");
            }

            functions.Add(func);
        }
    }
}
