﻿namespace Singlarity.SepaEngine
{
    public interface ISepa
    {
        ISepaInputTerminal[] Input { get; }
        ISepaOutputTerminal[] Output { get; }
        byte Reference { get; }
        ChakterTipo Afinidade { get; }
    }
}