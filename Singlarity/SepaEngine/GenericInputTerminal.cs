﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    [Localizable(typeof(ISepaInputTerminal))]
    public class GenericInputTerminal : ISepaInputTerminal
    {
        public TypeOfSepaInputTerminal TerminalType { get; set; }
        public SepaValue Value { get; set; }

        public GenericInputTerminal()
        {
            TerminalType = new TypeOfSepaInputTerminal();
            Value = new SepaValue();
        }
    }
}
