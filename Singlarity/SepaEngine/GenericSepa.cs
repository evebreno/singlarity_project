﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    public class GenericSepa : ISepa
    {
        public byte Reference { get; private set; }
        public ISepaInputTerminal[] Input { get; private set; }
        public ISepaOutputTerminal[] Output { get; private set; }
        public ChakterTipo Afinidade { get; private set; }

        public GenericSepa(byte reference, ISepaInputTerminal[] input, ISepaOutputTerminal[] output, ChakterTipo afinidade)
        {
            Reference = reference;
            Input = input;
            Output = output;
            Afinidade = afinidade;
        }
    }
}
