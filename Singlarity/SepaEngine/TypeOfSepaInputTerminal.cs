﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    [Flags]public enum TypeOfSepaInputTerminal
    {
        Environment=1,
        Sepa=2,
        Both=3
    }
}
