﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    public interface ISepaCompleteReferenceData : IGenre
    {
        byte Reference { get; }
        ISepaInputTerminal[] Input { get; }
        ISepaOutputTerminal[] Output { get; }
        ChakterTipo Afinidade { get; }

        bool ValidadeOutput(ISepaInputTerminal[] checkInput);
        ISepaOutputTerminal[] CalculateOutput(ISepaInputTerminal[] input);
    }
}
