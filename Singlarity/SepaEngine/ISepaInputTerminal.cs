﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    public interface ISepaInputTerminal
    {
        TypeOfSepaInputTerminal TerminalType { get; set; }
        SepaValue Value { get; set; }
    }

    public struct SepaValue
    {
        public bool Value { get; set; }
        public TypeOfSepaInputTerminal Origin { get; set; }

        public SepaValue(TypeOfSepaInputTerminal origin)
        {
            Value = false;
            Origin = origin;
        }

        public SepaValue(bool value, TypeOfSepaInputTerminal origin)
        {
            Value = value;
            Origin = origin;
        }
    }
}
