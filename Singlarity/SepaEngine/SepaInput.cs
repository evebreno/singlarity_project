﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity;
using Singlarity.SepaEngine;

namespace SepaReference.SepaEngine
{
    internal class SepaInput : ISepaInputTerminal
    {
        public TypeOfSepaInputTerminal TerminalType { get; set; }
        public SepaValue Value { get; set; }

        public SepaInput()
        {
            TerminalType = new TypeOfSepaInputTerminal();
            Value = new SepaValue();
        }

        public SepaInput(TypeOfSepaInputTerminal terminalInput, SepaValue value)
        {
            TerminalType = terminalInput;
            Value = value;
        }
    }
}
