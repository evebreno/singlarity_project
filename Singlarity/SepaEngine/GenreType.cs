﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    public enum GenreType
    {
        Neutro = 0,
        Alder = 1,
        Indigo,
        Vertice,
        Nupla,
        Balta,
        Pilar,
        Malte
    }
}
