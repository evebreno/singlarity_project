﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Singlarity.ChakterReference;

namespace Singlarity.SepaEngine
{
    static public partial class SepaCatalog
    {
        static public ISepaCompleteReferenceData[] Sepas { get { if (!_gathererd) GatherReferences(); return _sepas.ToArray(); } }
        static public ISepaOutputTerminal[] GetOutputs(byte sepaReference, ISepaInputTerminal[] Inputs)
        {
            foreach (var sepa in Sepas)
            {
                if (sepa.Reference == sepaReference)
                { return sepa.CalculateOutput(Inputs); }
            }

            throw new Exception("Não foi encontrado a sepa selecionada!");
        }

        static private List<ISepaCompleteReferenceData> _sepas = new List<ISepaCompleteReferenceData>();
        static private bool _gathererd = false;
        static private void GatherReferences()
        {
            Type sepaCatalog = typeof(SepaCatalog);
            MemberInfo[] membros = sepaCatalog.FindMembers(MemberTypes.NestedType, BindingFlags.NonPublic, new MemberFilter(DelegateToSearchCriteria), typeof(ISepaCompleteReferenceData));

            foreach (var membro in membros)
            {
                VisibleSepaAttribute attr = membro.GetCustomAttribute<VisibleSepaAttribute>();
                Type m = attr.Tipo;
                ConstructorInfo construct = m.GetConstructor(Type.EmptyTypes);

                _sepas.Add(construct.Invoke(new object[] { }) as ISepaCompleteReferenceData);
            }

            _gathererd = true;
        }
        private static bool DelegateToSearchCriteria(MemberInfo objMemberInfo, Object objSearch)
        {
            Type memb = objMemberInfo.GetType();
            VisibleSepaAttribute attr = objMemberInfo.GetCustomAttribute<VisibleSepaAttribute>();

            if (attr != null) return true;

            return false;
        }


        static public bool CheckExistence(byte refer)
        {
            foreach (var sepa in Sepas)
            {
                if (sepa.Reference == refer) return true;
            }

            return false;
        }
        static public bool CheckExistence(byte refer, ChakterTipo filter)
        {
            foreach (var sepa in Sepas)
            {
                if (sepa.Reference == refer)
                {
                    return sepa.Afinidade.HasFlag(filter);
                }
            }

            return false;
        }
        static public bool CheckModuleConsistence(byte[] modulation, ChakterTipo afinidade, out int errorIndex)
        {
            /* funcionamento:
             * primeiro verifica se a referencia é valida para o Chakter escolhido
             * verifica a disponibilidade de terminais na seguinte prioridade:
             * 1 - Terminais sepas disponíveis (se a entrada da sepa aceitar)
             * 2 - Terminais de ambiente disponíveis
             * O mecanismo procura deixar os terminais de ambiente para uso somente em ultimo caso.
             */
            int avaliableEnvironmentInputs = ChakterCatalog.GetEnvironmentInputs(afinidade);
            int avaliableSepaOutput = 0;
            int initialError = -1;
            bool operationResult = true;
            for (int i = 0; i < modulation.Length; i++)
            {
                if (CheckExistence(modulation[i], afinidade))
                {
                    var tmpTerminal = GetSepa(modulation[i]).Input;
                    foreach (var terminal in tmpTerminal)
                    {
                        if (terminal.TerminalType.HasFlag(TypeOfSepaInputTerminal.Sepa) && avaliableSepaOutput > 0)
                        {
                            avaliableSepaOutput--;
                        }

                        else if (terminal.TerminalType.HasFlag(TypeOfSepaInputTerminal.Environment))
                        {
                            if (avaliableEnvironmentInputs > 0) avaliableEnvironmentInputs--;
                            else { initialError = i; operationResult = false; goto BreakThrough; }
                        }

                        else
                        {
                            initialError = i;
                            operationResult = false;
                            goto BreakThrough;
                        }
                    }
                }

                else
                {
                    initialError = i;
                    operationResult = false;
                    goto BreakThrough;
                }

                avaliableSepaOutput += GetSepa(modulation[i]).Output.Length;
            }

            BreakThrough:;
            errorIndex = initialError;
            return operationResult;
        }
        static public ISepaCompleteReferenceData GetSepa(byte reference)
        {
            foreach (var sepa in Sepas)
            {
                if (sepa.Reference == reference) return sepa;
            }

            return null;
        }
        static public float GetDefaultCaracteristicValue(byte[] module, ChakterTipo afinidade, SepaValue[] environmentsValue)
        {
            if(!CheckModuleConsistence(module, afinidade, out int error)) { throw new Exception("Módulo inconsistente!"); }

            float result = 1;
            int environmentIndex = 0, sepaOutputIndex = 0 ;
            List<ISepaOutputTerminal> avaliableOutputs = new List<ISepaOutputTerminal>();
            for (int i = 0; i < module.Length; i++)
            {
                var tmpterminal = GetSepa(module[i]).Input;
                for(int j=0;j<tmpterminal.Length;j++)
                {
                    if (tmpterminal[j].TerminalType.HasFlag(TypeOfSepaInputTerminal.Sepa) && avaliableOutputs.Count > sepaOutputIndex)
                    {
                        tmpterminal[j].Value = new SepaValue(avaliableOutputs[sepaOutputIndex].Value, TypeOfSepaInputTerminal.Sepa);
                        sepaOutputIndex++;
                    }

                    else if (tmpterminal[j].TerminalType.HasFlag(TypeOfSepaInputTerminal.Environment) && environmentIndex < environmentsValue.Length)
                    {
                        tmpterminal[j].Value = environmentsValue[j];
                        environmentIndex++;
                    }

                    else throw new Exception("Erro durante o cálculo do valor!");
                }

                ISepaOutputTerminal[] outputs = GetOutputs(module[i], tmpterminal);
                foreach(var newOut in outputs)
                {
                    result += newOut.AbsoluteCaracteristicSumOutput;
                    result *= (1 + newOut.PercentageCaracteristicSumOutput);
                    //TODO: Implementar saída de vizinhaçã

                    avaliableOutputs.Add(newOut);
                }
            }

            return result;
        }
    }
}
