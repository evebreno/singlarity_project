﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    sealed class VisibleSepaAttribute : Attribute
    {
        /// <summary>
        /// Faz a classe ser visível para o motor de sepa
        /// </summary>
        /// <param name="Objeto">Classe a ser retornada para o motor</param>
        public VisibleSepaAttribute(Type Objeto)
        {
            _tipo = Objeto;
        }

        private Type _tipo;
        public Type Tipo { get { return _tipo; } }
    }
}
