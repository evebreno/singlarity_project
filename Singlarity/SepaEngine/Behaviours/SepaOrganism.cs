﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    static public partial class SepaCatalog
    {
        private class SepaOrganism
        {
            public virtual bool CheckConsistency(ISepaInputTerminal[] inputs, int inputlength, TypeOfSepaInputTerminal[] terminals)
            {
                bool consistence = true;

                if (inputs.Length != inputlength || inputlength != terminals.Length) { consistence = false; goto Finish; }

                for (int i = 0; i < inputs.Length; i++)
                {
                    if (!((inputs[i].Value.Origin & terminals[i]) == inputs[i].Value.Origin)) { consistence = false; goto Finish; }
                }

                Finish:;
                return consistence;
            }
        }
    }
}
