﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    static public partial class SepaCatalog
    {
        /// <summary>
        /// Sepa 40: >2;[1 (a*b)
        /// </summary>
        /// <param name="inputs">>2{11}{11}</param>
        /// <returns>(a*b) |0-> [0|0|0] |1-> [0|0.01%|0]</returns>
        [VisibleSepa(typeof(S_40))]
        private class S_40 : SepaOrganism, ISepaCompleteReferenceData
        {
            public byte Reference => 40;
            public GenreType Genre => GenreType.Neutro;

            public ISepaInputTerminal[] Input
            {
                get
                {
                    TypeOfSepaInputTerminal[] terminals = { TypeOfSepaInputTerminal.Both, TypeOfSepaInputTerminal.Both };
                    ISepaInputTerminal[] result = new ISepaInputTerminal[terminals.Length];

                    for(int i=0;i<result.Length;i++)
                    {
                        result[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        result[i].TerminalType = terminals[i];
                        result[i].Value = new SepaValue();
                    }
                    return result;
                }
            }
            public ISepaOutputTerminal[] Output
            {
                get
                {
                    ISepaOutputTerminal[] result = new ISepaOutputTerminal[1];
                    result.Initialize();

                    return result;
                }
            }
            public ChakterTipo Afinidade => (ChakterTipo)63;

            public ISepaOutputTerminal[] CalculateOutput(ISepaInputTerminal[] input)
            {
                ISepaOutputTerminal[] result = { ServiceLocator<ISepaOutputTerminal>.SearchClass() };

                TypeOfSepaInputTerminal[] terminals = new TypeOfSepaInputTerminal[Input.Length];
                for (int i = 0; i < Input.Length; i++) { terminals[i] = Input[i].TerminalType; }


                if (CheckConsistency(input, input.Length, terminals))
                {
                    result[0].Value = input[0].Value.Value & input[1].Value.Value;
                    result[0].AbsoluteCaracteristicSumOutput = 0;
                    result[0].PercentageCaracteristicSumOutput = (result[0].Value) ? 0.01f : 0;

                    return result;
                }
                else throw new Exception("Entrada inconsistente!");
            }

            public bool ValidadeOutput(ISepaInputTerminal[] checkInput)
            {
                TypeOfSepaInputTerminal[] terminals = new TypeOfSepaInputTerminal[Input.Length];
                for(int i=0;i<Input.Length;i++) { terminals[i] = Input[i].TerminalType; }

                return base.CheckConsistency(checkInput, Input.Length, terminals);
            }

            public S_40() { }
        }
    }
}
