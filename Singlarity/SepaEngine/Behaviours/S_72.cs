﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    static public partial class SepaCatalog
    {
        /// <summary>
        /// Sepa: 72 >1;[2
        /// </summary>
        /// <param name="inputs">1{10}</param>
        /// <returns>(a) |0-> [0.01|0|0] |1-> [0.01|0|0]</returns>
        [VisibleSepa(typeof(S_72))]
        private class S_72 : SepaOrganism, ISepaCompleteReferenceData
        {
            public byte Reference => 72;
            public GenreType Genre => GenreType.Nupla;

            public ISepaInputTerminal[] Input
            {
                get
                {
                    TypeOfSepaInputTerminal[] terminals = { TypeOfSepaInputTerminal.Sepa };
                    ISepaInputTerminal[] result = new ISepaInputTerminal[terminals.Length];

                    for (int i = 0; i < result.Length; i++)
                    {
                        result[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        result[i].TerminalType = terminals[i];
                        result[i].Value = new SepaValue();
                    }
                    return result;
                }
            }
            public ISepaOutputTerminal[] Output
            {
                get
                {
                    ISepaOutputTerminal[] result = new ISepaOutputTerminal[2];
                    result.Initialize();

                    return result;
                }
            }
            public ChakterTipo Afinidade => (ChakterTipo)63;

            public ISepaOutputTerminal[] CalculateOutput(ISepaInputTerminal[] input)
            {
                ISepaOutputTerminal[] result = new ISepaOutputTerminal[Output.Length];
                for(int i=0;i<Output.Length;i++)
                { result[i] = ServiceLocator<ISepaOutputTerminal>.SearchClass(); }

                TypeOfSepaInputTerminal[] terminals = new TypeOfSepaInputTerminal[Input.Length];
                for (int i = 0; i < Input.Length; i++) { terminals[i] = Input[i].TerminalType; }


                if (CheckConsistency(Input, Input.Length, terminals))
                {
                    foreach (var output in result)
                    {
                        output.Value = input[0].Value.Value;
                        output.AbsoluteCaracteristicSumOutput = -0.01f;
                        output.PercentageCaracteristicSumOutput = 0;
                    }

                    return result;
                }
                else throw new Exception("Entrada inconsistente!");
            }

            public bool ValidadeOutput(ISepaInputTerminal[] checkInput)
            {
                TypeOfSepaInputTerminal[] terminals = new TypeOfSepaInputTerminal[Input.Length];
                for (int i = 0; i < Input.Length; i++) { terminals[i] = Input[i].TerminalType; }

                return base.CheckConsistency(checkInput, Input.Length, terminals);
            }

            public S_72() { }
        }
    }
}
