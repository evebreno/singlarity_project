﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    [Localizable(typeof(ISepaOutputTerminal))]
    public class SepaOutput : ISepaOutputTerminal
    {
        public bool Value { get; set; }
        public float PercentageCaracteristicSumOutput { get; set; }
        public float AbsoluteCaracteristicSumOutput { get; set; }
        short ISepaOutputTerminal.SepaOutput { get; set; }
    }
}