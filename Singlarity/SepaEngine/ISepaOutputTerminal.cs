﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.SepaEngine
{
    public interface ISepaOutputTerminal
    {
        bool Value { get; set; }
        /// <summary>
        /// Valor percentual de acrescimo da característica em relação ao valor atual
        /// </summary>
        float PercentageCaracteristicSumOutput { get; set; }
        /// <summary>
        /// Valor absoluto de acréscimo da característica
        /// </summary>
        float AbsoluteCaracteristicSumOutput { get; set; }
        /// <summary>
        /// Representa a geração ou destruição da sepa atual/vizinha
        /// </summary>
        short SepaOutput { get; set; }
    }
}