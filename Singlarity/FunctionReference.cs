﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    [Localizable(typeof(IMainBoardFunction))]
    public class FunctionReference : IMainBoardFunction
    {
        public string Function { get; set; }
        public string Definition { get; set; }
        public MainBoardFunctionDelegation Delegation { get; set; }

        public FunctionReference()
        {
            Function = null;
            Definition = null;
            Delegation = new MainBoardFunctionDelegation();
        }

        public FunctionReference(string function, string definition, MainBoardFunctionDelegation requirement)
        {
            Function = function;
            Definition = definition;
            Delegation = requirement;
        }
    }
}
