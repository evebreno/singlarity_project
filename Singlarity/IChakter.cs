﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public interface IChakter
    {
        /// <summary>
        /// Nome da característica
        /// </summary>
        string Def { get; set; }
        string Descricao { get; set; }
        float Caracteristica { get; }
        ChakterTipo Tipo { get; set; }
        byte[] Sepa { get; set; }
    }
}
