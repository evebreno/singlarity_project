﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference;

namespace Singlarity
{
    [Localizable(typeof(IChakter))]
    public class ChakterDefault : IChakter
    {
        public string Def { get; set; }
        public string Descricao { get; set; }
        public float Caracteristica
        {
            get
            {
                if (_queries < ChakterCatalog.GetQueryTimes(this.Tipo)) _queries++;
                else { _char = CalculateChar(); _queries = 0; }

                return _char;
            }
        }
        public ChakterTipo Tipo { get; set; }
        public byte[] Sepa { get; set; }

        private int _queries = int.MaxValue;
        private float _char = 1;

        public ChakterDefault()
        {
            Def = "";
            Descricao = "";
            Tipo = new ChakterTipo();
            Sepa = new byte[0];
        }
        public ChakterDefault(string def, string descricao,byte[] sepa, float caracteristica, ChakterTipo signo)
        {
            Def = def;
            Descricao = descricao;
            _char = caracteristica;
            Tipo = signo;
            Sepa = sepa;
         }

        private float CalculateChar()
        {
            ISepaInputTerminal[] terminals = ChakterCatalog.GetEnvironmentInputsValue(Tipo);
            SepaValue[] values = new SepaValue[terminals.Length];
            for(int i=0;i<terminals.Length;i++)
            {
                values[i] = terminals[i].Value;
            }

           return SepaCatalog.GetDefaultCaracteristicValue(Sepa, Tipo, values);
        }
    }
}
