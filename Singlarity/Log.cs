﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    [RequiresInitialization("SetPresentation")]
    static class Log
    {
        static public bool detail = false;
        static public void SetPresentation()
        {
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Detail_f, Commands_PTBR.Detail_d, new MainBoardFunctionDelegation(typeof(Log), "ToogleDetail") }));
        }

        static public void ToogleDetail()
        {
            detail = detail ? false : true;
        }

        static public void Show(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(texto);
        }
        static public void ShowLine(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(texto);
            ResetColor();
        }
        static public void ShowWarning (string texto)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(texto);
        }
        static public void ShowError(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(texto);
        }
        static public void ShowWarningLine(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(texto);
            ResetColor();
        }
        static public void ShowErrorLine(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(texto);
            ResetColor();
        }
        static public void ShowDetail(string texto)
        {
            if (detail)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(texto);
            }
        }
        static public void ShowDetailLine(string texto)
        {
            if (detail)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(texto);
                ResetColor();
            }
        }

        static private void ResetColor()
        {
            Console.ResetColor();
        }
    }
}
