﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Singlarity
{
    [Localizable(typeof(testinho), Tag = "valeu")]
    public class TestClass : testinho
    {
        private string _func;
        public string Func => _func;

        public TestClass(string testao)
        {
            _func = testao;
        }

        static public void Teste()
        {
            IMainBoardFunction referencia = ServiceLocator<IMainBoardFunction>.SearchClass();
            referencia.Function = "teste";
            referencia.Definition = "Testa a função";
            referencia.Delegation = new MainBoardFunctionDelegation(typeof(TestClass), "FZR");
            MainBoard.AddFunction(referencia);
        }

        static public void FZR()
        {
            Console.WriteLine("FZR Funfou!");
        }
    }

    public interface testinho
    {
        string Func { get; }
    }
}
