﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Files
{
    [Localizable(typeof(IMakeFile))]
    public class MakeFile : IMakeFile
    {
        public byte[] Output { get => actualStream.ToArray(); private set => _output = value; }

        private List<byte> actualStream;
        private byte[] _output;

        public MakeFile()
        {
            actualStream = new List<byte>();
            _output = new byte[0];
        }
        public MakeFile(byte[] preFormedBytes)
        {
            actualStream = new List<byte>();
            foreach(byte b in preFormedBytes)
            { actualStream.Add(b); }

            _output = actualStream.ToArray();
        }

        public void InsertByte(byte byteToInsert)
        {
            actualStream.Add(byteToInsert);
        }
        public void InsertBytes(byte[] byteToInsert)
        {
            foreach(byte b in byteToInsert) { actualStream.Add(b); }
        }
        static public byte[] GetJustFormatedStandardHeader()
        {
            List<byte> bytes = new List<byte>();
            byte[] bin1 = Encoding.Default.GetBytes(FileRes.AppName);
            byte[] bin2 = Encoding.Default.GetBytes(FileRes.AppVersion);

            bytes.Add(40);
            foreach(byte b in bin1) { bytes.Add(b); }
            bytes.Add(255);
            bytes.Add(41);
            foreach (byte b in bin2) { bytes.Add(b); }
            bytes.Add(255);
            return bytes.ToArray();
        }
    }
}
