﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Singlarity.Files
{
    public class StandardEncoder
    {
        public byte[] AppName { get { UpdateAppName(); return _appName; } private set => _appName = value; }
        public byte[] AppVersion { get { UpdateAppVersion(); return _appVersion; } private set => _appVersion = value; }

        private byte[] _appName;
        private byte[] _appVersion;

        private void UpdateAppName()
        {
            _appName = Encoding.Default.GetBytes(FileRes.AppName);
        }
        private void UpdateAppVersion()
        {
            _appVersion = Encoding.Default.GetBytes(Application.ProductVersion);
        }

        static public byte[] GetBytes(string valueToGet)
        {
            return Encoding.Default.GetBytes(valueToGet);
        }
        static public byte[] GetBytes(int valueToGet)
        {
            return Encoding.Default.GetBytes(valueToGet.ToString());
        }
        static public byte[] GetBytes(float valueToGet)
        {
            return Encoding.Default.GetBytes(valueToGet.ToString());
        }

        static public int GetInt(byte[] value)
        {
            return int.Parse(Encoding.Default.GetString(value));
        }
        static public float GetFloat(byte[] value)
        {
            return float.Parse(Encoding.Default.GetString(value));
        }
        static public string GetString(byte[] value)
        {
            return Encoding.Default.GetString(value);
        }

    }
}