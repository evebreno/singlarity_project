﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Singlarity;
using Singlarity.ChipEngine;
using Singlarity.User;

namespace Singlarity.Files
{
    /// <summary>
    /// Armazena e recupera as informações dos chips
    /// </summary>
    static class ChipFile
    {
        static public IMakeFile FileToWrite { get; set; }
        static public IReadFile FileToRead { get; set; }

        static public void SaveChipsData(Chip[] data)
        {
            foreach (Chip actualChip in data)
            {
                FileToWrite = ServiceLocator<IMakeFile>.SearchClass(null, new object[] { MakeFile.GetJustFormatedStandardHeader() });

                FileToWrite.InsertByte(13);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(actualChip.Nome));
                FileToWrite.InsertByte(255);

                FileToWrite.InsertByte(14);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes((int)actualChip.Signo));
                FileToWrite.InsertByte(255);

                FileToWrite.InsertByte(17);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(actualChip.Cor.Name));
                FileToWrite.InsertByte(255);

                FileToWrite.InsertByte(22);
                foreach(IChakter caracteristica in actualChip.Carac)
                {
                    FileToWrite.InsertByte(23);
                    FileToWrite.InsertBytes(StandardEncoder.GetBytes(caracteristica.Def));
                    FileToWrite.InsertByte(255);

                    FileToWrite.InsertByte(27);
                    FileToWrite.InsertBytes(StandardEncoder.GetBytes(caracteristica.Descricao));
                    FileToWrite.InsertByte(255);

                    FileToWrite.InsertByte(28);
                    FileToWrite.InsertBytes(StandardEncoder.GetBytes((int)caracteristica.Tipo));
                    FileToWrite.InsertByte(255);

                    FileToWrite.InsertByte(29);
                    FileToWrite.InsertBytes(caracteristica.Sepa);
                    FileToWrite.InsertBytes(new byte[] { 0, 1, 2, 3, 255 });
                }
                FileToWrite.InsertByte(254);

                if (SetupDirectories.CheckDirectories())
                {
                    Log.ShowDetailLine("Diretórios ok.");
                    try
                    {
                        Log.ShowDetailLine("Tentando escrever arquivo no disco...");
                        FileStream escritor = new FileStream($"{SetupDirectories.UserChips}/{actualChip.Nome}.sscf", FileMode.OpenOrCreate);
                        escritor.Write(FileToWrite.Output, 0, FileToWrite.Output.Length);

                        Log.ShowDetailLine("Informações salvas com sucesso!");
                        escritor.Close();
                    }
                    catch (Exception e)
                    {
                        Log.ShowErrorLine("Não foi possível gravar as informações de usuário: " + e.Message);
                    }
                }
            }
        }

        static public Chip[] RetriveChipData()
        {
            string[] files = Directory.GetFiles(SetupDirectories.UserChips, "*.sscf", SearchOption.TopDirectoryOnly);
            List<Chip> chips = new List<Chip>(files.Length);

            foreach(string actualFile in files)
            {
                FileStream leitor = new FileStream(actualFile, FileMode.OpenOrCreate);
                var buffer = new byte[leitor.Length];

                leitor.Read(buffer, 0, buffer.Length);
                FileToRead.Input = buffer;
                FileToRead.ActualPosition = 0;

                FileToRead.JumpStandardHeader();
                if (!FileToRead.CheckFirst(13, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                string usrName = StandardEncoder.GetString(FileToRead.ReadUntil(255, true));

                if (!FileToRead.CheckFirst(14, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                Sigma signo = (Sigma)StandardEncoder.GetInt(FileToRead.ReadUntil(255, true));

                if (!FileToRead.CheckFirst(17, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                string colorSchema = StandardEncoder.GetString(FileToRead.ReadUntil(255, true));

                if (!FileToRead.CheckFirst(22, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }

                if (!FileToRead.CheckFirst(23, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }

                List<IChakter> chakis = new List<IChakter>();
                bool stop = false;
                while(!stop)
                {
                    string chakisNom = StandardEncoder.GetString(FileToRead.ReadUntil(255, true));

                    if (!FileToRead.CheckFirst(27, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    string chakisdesc = StandardEncoder.GetString(FileToRead.ReadUntil(255, true));

                    if (!FileToRead.CheckFirst(28, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    ChakterTipo chakistip = (ChakterTipo)StandardEncoder.GetInt(FileToRead.ReadUntil(255, true));

                    if (!FileToRead.CheckFirst(29, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    byte[] sepa = FileToRead.ReadUntil(new byte[] { 0,1,2,3,255}, true);

                    chakis.Add(new ChakterDefault(chakisNom, chakisdesc, sepa, 1, chakistip));
                    if (FileToRead.CheckFirst(23, true)) { continue; }
                    else if (FileToRead.CheckFirst(254, false)) { stop=true; }
                    else throw new Exception("Arquivo corrompido, ou com dados alterados!");
                }
                chips.Add(new Chip(usrName, signo, chakis.ToArray(), ChipColorData.GetSchemaByName(colorSchema)));
            }

            return chips.ToArray();
        }
    }
}
