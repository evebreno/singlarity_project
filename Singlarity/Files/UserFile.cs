﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Singlarity.Files
{
    /// <summary>
    /// Armazena e recupera as informações de usuário
    /// </summary>
    static class UserFile
    {
        static public IMakeFile FileToWrite { get; set; }
        static public IReadFile FileToRead { get; set; }

        static public void SaveUserData(IUser Usuario)
        {
            if(Usuario.Nome == null || Usuario.Nome == "") Log.ShowErrorLine("Usuário não encontrado, o sistema não irá salvar");

            else
            {
                Log.ShowDetailLine("Preparando arquivo...");
                FileToWrite = ServiceLocator<IMakeFile>.SearchClass(null, new object[] { MakeFile.GetJustFormatedStandardHeader() });
                byte[] metadata = { 10, 13, 14, 17, 18, 255 };

                FileToWrite.InsertByte(metadata[1]);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(Usuario.Nome));
                FileToWrite.InsertByte(metadata[5]);

                FileToWrite.InsertByte(metadata[2]);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(Usuario.Life));
                FileToWrite.InsertByte(metadata[5]);

                FileToWrite.InsertByte(metadata[2]);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(Usuario.Coin));
                FileToWrite.InsertByte(metadata[5]);

                FileToWrite.InsertByte(metadata[3]);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(Usuario.CreditMargin));
                FileToWrite.InsertByte(metadata[5]);

                FileToWrite.InsertByte(metadata[4]);
                FileToWrite.InsertBytes(StandardEncoder.GetBytes(Usuario.Chips.Length));
                FileToWrite.InsertByte(metadata[5]);

                FileToWrite.InsertByte(metadata[0]);
                FileToWrite.InsertByte(metadata[5]);

                Log.ShowDetailLine("Arquivo pronto, o sistema irá salvá-lo agora.");
                Log.ShowDetailLine("Verificando a existencia dos diretórios...");
                if(SetupDirectories.CheckDirectories())
                {
                    Log.ShowDetailLine("Diretórios ok.");
                    try
                    {
                        Log.ShowDetailLine("Tentando escrever arquivo no disco...");
                        FileStream escritor = new FileStream($"{SetupDirectories.UserDir}/{FileRes.UserFileName}", FileMode.OpenOrCreate);
                        escritor.Write(FileToWrite.Output, 0, FileToWrite.Output.Length);

                        Log.ShowDetailLine("Informações salvas com sucesso!");
                        escritor.Close();
                    }
                    catch (Exception e)
                    {
                        Log.ShowErrorLine("Não foi possível gravar as informações de usuário: " + e.Message);
                    }
                }

                else
                {
                    Log.ShowDetailLine("Alguns dos diretórios não existem, o sistema irá começar um processo de criação dos diretórios agora...");
                    SetupDirectories.SetupEachDir();
                    
                    Log.ShowDetailLine("Diretórios ok.");
                    try
                    {
                        Log.ShowDetailLine("Tentando escrever arquivo no disco...");
                        FileStream escritor = new FileStream($"{SetupDirectories.UserDir}/{FileRes.UserFileName}", FileMode.OpenOrCreate);
                        escritor.Write(FileToWrite.Output, 0, FileToWrite.Output.Length);

                        Log.ShowDetailLine("Informações salvas com sucesso!");
                        escritor.Close();
                    }
                    catch (Exception e)
                    {
                        Log.ShowErrorLine("Não foi possível gravar as informações de usuário: " + e.Message);
                    }
                }
            }
        }

        static public IUser RetrieveUserData()
        {
            Log.ShowDetailLine("Verificando a existencia dos diretórios...");
            byte[] buffer;
            if (SetupDirectories.CheckDirectories())
            {
                Log.ShowDetailLine("Diretórios ok.");
                try
                {
                    FileStream escritor = new FileStream($"{SetupDirectories.UserDir}/{FileRes.UserFileName}", FileMode.OpenOrCreate);
                    buffer = new byte[escritor.Length];

                    escritor.Read(buffer, 0, buffer.Length);
                    FileToRead.Input = buffer;
                    FileToRead.ActualPosition = 0;

                    FileToRead.JumpStandardHeader();
                    if (!FileToRead.CheckFirst(13, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    string usrName = StandardEncoder.GetString(FileToRead.ReadUntil(255, true));

                    if (!FileToRead.CheckFirst(14, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    float usrLife = StandardEncoder.GetFloat(FileToRead.ReadUntil(255, true));

                    if (!FileToRead.CheckFirst(14, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    float usrCoin = StandardEncoder.GetFloat(FileToRead.ReadUntil(255, true));

                    if (!FileToRead.CheckFirst(17, true)) { throw new Exception("Arquivo corrompido, ou com dados alterados!"); }
                    float usrCredit = StandardEncoder.GetFloat(FileToRead.ReadUntil(255, true));

                    return Singlarity.User.Player.GetPlayer(usrName, usrCoin, usrCredit, null, usrLife);
                }
                catch (Exception e)
                {
                    Log.ShowDetailLine("Erro durante a leitura dos dados: " + e.Message);
                    Log.ShowDetailLine("A operação foi abortada.");

                    throw new Exception();
                }
            }
            else return null;
        }
    }
}