﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Singlarity.Files
{
    public class SetupDirectories
    {
        static public string UserDir { get { return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}{FileRes.UserShrtPath}"; } }
        static public string UserPref { get { return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}{FileRes.PrefsShrtPath}"; } }
        static public string UserChips { get { return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}{FileRes.ChipsShrtPath}"; } }

        static public bool CheckDirectories()
        {
            bool exist = true;
            if (!Directory.Exists(UserDir)) exist = false;
            if (!Directory.Exists(UserPref)) exist = false;
            if (!Directory.Exists(UserChips)) exist = false;

            return exist;
        }

        /// <summary>
        /// Cria os diretórios inexistentes
        /// </summary>
        /// <returns>Número de diretórios criados</returns>
        static public void SetupEachDir()
        {
            try
            {
                Directory.CreateDirectory($"{UserDir}");
                Directory.CreateDirectory($"{UserPref}");
                Directory.CreateDirectory($"{UserChips}");
            }
            catch (Exception e)
            {
                Log.ShowErrorLine("Não foi possível criar os diretórios: " + e.Message);
            }
        }
    }
}
