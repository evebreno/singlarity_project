﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameThor;
using Singlarity.ChipEngine;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference;


namespace Singlarity.Enemy
{
    [Localizable(typeof(IEnemyBuilder))]
    public class EnemyBuilder : IEnemyBuilder
    {
        public IEnemy BuildEnemy()
        {
            IEnemy enemy = ServiceLocator<IEnemy>.SearchClass();
            enemy.Nome = GenerateName();
            enemy.Chips = new Chip[] { GenerateChip() };
            enemy.Coin = CalculateCoin(enemy.Chips);

            return enemy;
        }

        /// <summary>
        /// Gera um nome de até 3 palavras
        /// </summary>
        private string GenerateName()
        {
            IName nome = NameThorServiceLocator.SearchNameGenerator();
            Random r = new Random();

            return nome.GenerateName(r.Next(1,4));
        }
        /// <summary>
        /// Gera um nome de acordo com o número de palavras especificado
        /// </summary>
        /// <param name="wordNumber">número de palavras a ser usado no nome.</param>
        private string GenerateName(int wordNumber)
        {
            IName nome = NameThorServiceLocator.SearchNameGenerator();

            return nome.GenerateName(wordNumber);
        }
        private Chip GenerateChip()
        {
            Random r = new Random();
            string Nome = GenerateName(1);
            Sigma signo = (Sigma)r.Next(6);
            ChipColor cor = ChipColorData.Esquemas[r.Next(ChipColorData.Esquemas.Length)];

            IChakMaker maker = ServiceLocator<IChakMaker>.SearchClass();
            IChakter[] chaks = new IChakter[r.Next(1, 7)];
            for(int i=0;i< chaks.Length; i++)
            {
                chaks[i] = maker.GenerateChak();
            }

            return new Chip(Nome, signo, chaks, cor);
        }
        /// <summary>
        /// Sugere um valor monetário de acordo com os chips
        /// </summary>
        /// <param name="chips">Chips a serem usados para calcular</param>
        /// <returns></returns>
        private float CalculateCoin(Chip[] chips)
        {
            float especulateMoney = 0;
            foreach (var chip in chips)
            {
                foreach (var chak in chip.Carac)
                {
                    for (int i = 0; i < 4; i++)
                    { especulateMoney += (float)Math.Pow(chak.Caracteristica, 2) / 100; }

                    especulateMoney += 5000 * (AnalyzeSepaComplexity(chak.Sepa));
                }
            }

            return especulateMoney;
        }

        /// <summary>
        /// Verifica a complexidade da Sepa
        /// </summary>
        /// <param name="sepaToAnalyze">Sepa a ser analisada</param>
        /// <returns>Valor de 0 a 1 representando a complexidade</returns>
        private float AnalyzeSepaComplexity(byte[] sepaToAnalyze)
        {
            float result = 1;
            result *= (sepaToAnalyze.Length) / 1024;

            int outputCount = 0, inputCount = 0;
            foreach(var by in sepaToAnalyze)
            {
                outputCount += SepaCatalog.GetSepa(by).Output.Length;
                inputCount += SepaCatalog.GetSepa(by).Input.Length;
            }

            result *= 1 + (outputCount / 512);
            result *= 1 + (inputCount / 256);

            return (result > 1) ? 1 : result;
        }
    }
}
