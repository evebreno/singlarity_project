﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Enemy
{
    [Localizable(typeof(IEnemy))]
    public class GenericEnemy : IEnemy
    {
        public string Nome { get; set; }
        public float Coin { get; set; }
        public Chip[] Chips { get; set; }

        public GenericEnemy()
        {
            Nome = null;
            Coin = new float();
            Chips = null;
        }
    }
}
