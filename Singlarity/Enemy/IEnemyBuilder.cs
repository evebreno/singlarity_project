﻿namespace Singlarity.Enemy
{
    public interface IEnemyBuilder
    {
        IEnemy BuildEnemy();
    }
}