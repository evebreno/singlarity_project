﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.Enemy
{
    public interface IEnemy
    {
        string Nome { get; set; }
        float Coin { get; set; }
        Chip[] Chips { get; set; }
    }
}
