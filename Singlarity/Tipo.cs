﻿using System;

namespace Singlarity
{
    [Flags]
    public enum ChakterTipo
    {
        /// <summary>
        /// 5 variáveis de ambiente
        /// </summary>
        versbundel = 1 << 0,
        /// <summary>
        /// 3 variáveis de ambiente
        /// </summary>
        wafungwa = 1 << 1,
        /// <summary>
        /// 6 variáveis de ambiente
        /// </summary>
        jäätelööä = 1 << 2,
        /// <summary>
        /// 13 variáveis de ambiente
        /// </summary>
        teksteillään = 1 << 3,
        /// <summary>
        /// 11 variáveis de ambiente
        /// </summary>
        nepalista = 1 << 4,
        /// <summary>
        /// 4 variáveis de ambiente
        /// </summary>
        inlewe = 1 << 5
    }
}
