﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Singlarity.ChakterReference;
using Singlarity.BattleEngine;
using BattleField;
using Singlarity.ChipEngine;

namespace Singlarity
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //Thread rs = new Thread(RandomName);
            //rs.SetApartmentState(ApartmentState.STA);
            //rs.Start();
            ProgramInitializationEngine.Initialize();
            ProgramInitializationEngine.PostInitialize();
        }

        private static void Teste()
        {
            ChakMaker maker = new ChakMaker();
            IChakter chak = maker.GenerateChak();

            Console.WriteLine($"Nome: {chak.Def}");
            Console.WriteLine($"Tipo: {chak.Tipo}");
            foreach (byte b in chak.Sepa) { Console.Write(b + " "); }

            while (true)
            {
                Console.WriteLine($"Valor da característica {chak.Caracteristica}");
                Console.ReadLine();
            }
        }

        static private void RandomName()
        {
            MainWindow mw = new MainWindow();
            var ap = new App();
            mw.ConsoleBlockSentDataEvent += Mw_ConsoleBlockSentDataEvent;
            mw.SetPlayerColorSchema(ChipColorData.Esquemas[7].ForeColor, ChipColorData.Esquemas[7].BackColor);
            ap.Run(mw);
        }

        private static void Mw_ConsoleBlockSentDataEvent(object sender, EventArgs e)
        {
            var data = e as ConsoleDataSentEventArgs;
            Console.WriteLine($"comando recebido {data.CommandSent}");
        }
    }
}
