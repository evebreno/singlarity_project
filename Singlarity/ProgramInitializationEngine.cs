﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Singlarity
{
    static public class ProgramInitializationEngine
    {
        static private List<InitializationReference> InitializationReferenceList { get; set; }

        static private void GetInitializationNeedList()
        {
            InitializationReferenceList = new List<InitializationReference>();
            Assembly a = Assembly.GetEntryAssembly();
            Type[] tipos = a.GetTypes();

            foreach (var tipo in tipos)
            {
                try
                {
                    RequiresInitializationAttribute[] attr = tipo.GetCustomAttributes<RequiresInitializationAttribute>() as RequiresInitializationAttribute[];
                    foreach (var attrAtual in attr)
                    {
                        if (attrAtual != null)
                        { InitializationReferenceList.Add(new InitializationReference(tipo, attrAtual.MethodName, attrAtual.InitializationOrder, attrAtual.InitializationTime)); }
                    }
                }
                catch { }
            }
        }

        static public void Initialize()
        {
            GetInitializationNeedList();

            for (int i = 0; i < 256; i++)
            {
                foreach (var initializationRequirement in InitializationReferenceList)
                {
                    if (initializationRequirement.InitializationPosition == RequiresInitializationAttribute.InitializationPosition.Initialization && 
                        initializationRequirement.InitializationOrder == i)
                    {
                        Type tipo = initializationRequirement.Refer;
                        MethodInfo metodo = tipo.GetMethod(initializationRequirement.InitializationMethod);

                        metodo.Invoke(null, new object[] { });
                    }
                }
            }
        }

        static public void PostInitialize()
        {
            for (int i = 0; i < 256; i++)
            {
                foreach (var initializationRequirement in InitializationReferenceList)
                {
                    if (initializationRequirement.InitializationPosition == RequiresInitializationAttribute.InitializationPosition.PostInitialization &&
                        initializationRequirement.InitializationOrder == i)
                    {
                        Type tipo = initializationRequirement.Refer;
                        MethodInfo metodo = tipo.GetMethod(initializationRequirement.InitializationMethod);

                        metodo.Invoke(null, new object[] { });
                    }
                }
            }
        }

        public struct InitializationReference
        {
            public Type Refer { get; private set; }
            public string InitializationMethod { get; private set; }
            public RequiresInitializationAttribute.InitializationPosition InitializationPosition { get; private set; }
            public byte InitializationOrder { get; private set; }

            public InitializationReference(Type refer, string method)
            {
                Refer = refer;
                InitializationMethod = method;
                InitializationPosition = RequiresInitializationAttribute.InitializationPosition.Initialization;
                InitializationOrder = 10;
            }
            public InitializationReference(Type refer, string method, byte initOrder, RequiresInitializationAttribute.InitializationPosition position)
            {
                Refer = refer;
                InitializationMethod = method;
                InitializationPosition = position;
                InitializationOrder = initOrder;
            }
        }
    }
}
