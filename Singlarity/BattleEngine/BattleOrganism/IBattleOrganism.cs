﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.BattleEngine.BattleOrganism
{
    public interface IBattleOrganism
    {
        bool AttackUser(IUser sender, IUser target, float value);
        event ReceivedDamage DamageSent;
    }
}
