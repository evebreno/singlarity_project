﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.BattleEngine.BattleOrganism
{
    public delegate void ReceivedDamage (IBattleChip target, float value);
}
