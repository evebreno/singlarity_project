﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.ChipEngine;

namespace Singlarity.BattleEngine
{
    public interface IBattleChip
    {
        float Life { get; set; }
        string Nome { get; set; }
        Sigma Signo { get; set; }
        IChakter[] Carac { get; set; }
        ChipColor Cor { get; set; }
    }

    static public class ChipExtension
    {
        static public IBattleChip GetIBattleChip (this Chip chip)
        {
            IBattleChip result = ServiceLocator<IBattleChip>.SearchClass();

            result.Life = 100;
            result.Nome = chip.Nome;
            result.Signo = chip.Signo;
            result.Carac = chip.Carac;
            result.Cor = chip.Cor;

            return result;
        }
    }
}
