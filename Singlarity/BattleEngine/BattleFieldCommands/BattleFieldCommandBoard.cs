﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.BattleEngine.BattleFieldCommands
{
    static class BattleFieldCommandBoard
    {
        static private List<BattleFieldFunction> functions = new List<BattleFieldFunction>();
        public static List<BattleFieldFunction> Functions { get => functions; set => functions = value; }

        public static void AddFunction(BattleFieldFunction functionToAdd)
        {
            foreach(var func in Functions)
            { if (func.Definition == functionToAdd.Definition) throw new Exception("Definição já adicionada!"); }

            functions.Add(functionToAdd);
        }
    }
}
