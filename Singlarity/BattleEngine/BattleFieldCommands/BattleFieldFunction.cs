﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.BattleEngine.BattleFieldCommands
{
    public delegate void BattleFieldFunctionDelegate(params string[] args);

    public struct BattleFieldFunction
    {
        public string Definition { get; set; }
        public string Delegation { get; set; }
        public BattleFieldFunctionDelegate Function { get; set; }

        public BattleFieldFunction(string definition, string delegation, BattleFieldFunctionDelegate function)
        {
            Definition = definition;
            Delegation = delegation;
            Function = function;
        }
    }
}
