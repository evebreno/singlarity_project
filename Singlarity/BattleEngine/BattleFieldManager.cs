﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using BattleField;
using Singlarity.Enemy;
using Singlarity.BattleEngine.BattleOrganism;

namespace Singlarity.BattleEngine
{
    /// <summary>
    /// Responsável por gerenciar o organismo de batalha
    /// </summary>
    public class BattleFieldManager
    {
        public IUser Player1 { get; private set; }
        public IEnemy Player2 { get; private set; } //TODO: Criar uma generalização. Evitar usar tipo inimigo e tipo player

        public IBattleChip ActualP1Chip { get; private set; }
        public IBattleChip ActualP2Chip { get; private set; }

        public /*BattleResult*/ void Battle(IUser player1, IEnemy player2, int initialP1Chip, int initialP2Chip)
        {
            Player1 = player1;
            Player2 = player2;
            ActualP1Chip = Player1.Chips[initialP1Chip].GetIBattleChip();
            ActualP2Chip = Player2.Chips[initialP2Chip].GetIBattleChip();

            Thread newThread = new Thread(CommunicateConsole);
            newThread.SetApartmentState(ApartmentState.STA);
            newThread.Start();
        }

        private void CommunicateConsole()
        {
            var p1 = ActualP1Chip;
            var p2 = ActualP2Chip;

            MainWindow mw = new MainWindow();
            mw.SetPlayerColorSchema(p1.Cor.ForeColor, p1.Cor.BackColor);
            mw.SetEnemyColorSchema(p2.Cor.ForeColor, p2.Cor.BackColor);

            mw.ConsoleBlockSentDataEvent += Mw_ConsoleBlockSentDataEvent;
            mw.WriteAtEnemyStory($"{Player2.Nome} usando {Player2.Chips[0].Nome}\nContém {Player2.Chips[0].Carac.Length} características.");

            var app = new App();
            app.Run(mw);
        }

        private void SingleBattleOrganism()
        {
            IBattleOrganism organism = ServiceLocator<IBattleOrganism>.SearchClass();
            organism.DamageSent += Organism_DamageSent;
        }

        private void Organism_DamageSent(IBattleChip target, float value)
        {
            if (ActualP1Chip.Nome == target.Nome)
            {
                ActualP1Chip.Life -= value;
            }
            else if (ActualP2Chip.Nome == target.Nome)
            {
                ActualP2Chip.Life -= value;
            }

            else throw new Exception("O alvo não foi encontrado!");
        }

        private void Mw_ConsoleBlockSentDataEvent(object sender, EventArgs e)
        {
            var es = e as ConsoleDataSentEventArgs;
            Console.Write($"foi escrito: {es.CommandSent}");
        }
    }
}
