﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.Enemy;

namespace Singlarity.BattleEngine
{
    /// <summary>
    /// Objeto onde se localiza os adversários diários
    /// </summary>
    static public class BattleCenter
    {
        //TODO: Colocar uma função com o resultado de determinada batalha para saber se o inimigo continua, ou vai embora
        static public IEnemy[] CasualEnemies
        {
            get
            {
                if (!casualEnemiesFound) { FindCasualEnemies(); }
                return _casualEnemies;
            }
        }

        static private IEnemy[] _casualEnemies;
        static private bool casualEnemiesFound = false;

        static private void FindCasualEnemies()
        {
            Random r = new Random(DateTime.Now.DayOfYear);
            _casualEnemies = new IEnemy[r.Next(1, 10)];
            IEnemyBuilder builder = ServiceLocator<IEnemyBuilder>.SearchClass();

            for (int i = 0; i < _casualEnemies.Length; i++)
            {
                _casualEnemies[i] = builder.BuildEnemy();
            }

            casualEnemiesFound = true;
        }
    }
}
