﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Singlarity.User;
using Singlarity.BattleBoard;
using Singlarity.Enemy;
using Singlarity.ChipEngine;
using BattleField;

namespace Singlarity.BattleEngine.SingleBattle
{
    [RequiresInitialization("SetPresentation")]
    static public class SingleBattleInitializer
    {         
        static public void SetPresentation()
        {
            var function = ServiceLocator<IMainBoardFunction>.SearchClass();
            function.Definition = BattleBoardCommands_PTBR.SingleBattle_d;
            function.Function = BattleBoardCommands_PTBR.SingleBattle_f;
            function.Delegation = new MainBoardFunctionDelegation(typeof(SingleBattleInitializer), "StartBattle");

            MainBattleBoard.AddFunction(function);
        }

        static public void StartBattle()
        {
            int enemySelected = SelectEnemy();
            int chipSelected = SelectedChip();

            //TODO: Invocar o BattleFieldManager para gerenciar a batalha
            var enemy = BattleCenter.CasualEnemies[enemySelected];
            BattleFieldManager bfm = new BattleFieldManager();
            bfm.Battle(Protagonista.GetUser(), enemy, chipSelected, 0);
        }

        static private int SelectEnemy()
        {
            IEnemy[] avaliableEnemies = BattleCenter.CasualEnemies;
            Log.ShowWarningLine("Adversários disponíveis");

            for (int i = 0; i < avaliableEnemies.Length; i++)
            {
                Log.ShowWarningLine($"\nNome: {avaliableEnemies[i].Nome}\t\t({i})");
                Log.ShowLine($"Chips: {avaliableEnemies[i].Chips.Length}");
                Log.ShowLine($"Dinheiro: {avaliableEnemies[i].Coin}");
            }

            Log.Show("Digite o número correspondente ao adversário: ");
            string command = Console.ReadLine();

            if(int.TryParse(command, out int result))
            {
                if(result>=0 && result< avaliableEnemies.Length)
                { return result; }

                else throw new Exception("Número escolhido inválido!");
            }

            else throw new Exception("Número escolhido inválido!");
        }

        static private int SelectedChip()
        {
            Log.ShowWarningLine("Selecione o chip a ser usado");

            try
            {
                for (int i = 0; i < Protagonista.Chips.Length; i++)
                {
                    var chip = Protagonista.Chips[i];
                    Console.BackgroundColor = chip.Cor.BackColor;
                    Console.ForegroundColor = chip.Cor.ForeColor;
                    Console.WriteLine($"{chip.Nome}\t{chip.Signo}\tChakters: {chip.Carac.Length}\t{i}");
                }
            }
            catch (NullReferenceException)
            { Log.ShowWarningLine("Não foi encontrado chip algum"); }
            finally { Console.ResetColor(); }


            Log.Show("Digite o número correspondente ao chip: ");
            string command = Console.ReadLine();

            if (int.TryParse(command, out int result))
            {
                if (result >= 0 && result < Protagonista.Chips.Length)
                { return result; }

                else throw new Exception("Número escolhido inválido!");
            }

            else throw new Exception("Número escolhido inválido!");
        }
    }
}
