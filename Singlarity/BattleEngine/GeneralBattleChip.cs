﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.ChipEngine;

namespace Singlarity.BattleEngine
{
    [Localizable(typeof(IBattleChip))]
    public class GeneralBattleChip : IBattleChip
    {
        public float Life { get; set ; }
        public string Nome { get; set; }
        public Sigma Signo { get; set; }
        public IChakter[] Carac { get; set; }
        public ChipColor Cor { get; set; }

        public GeneralBattleChip()
        {
            Life = new float();
            Nome = null;
            Signo = new Sigma();
            Carac = new IChakter[0];
            Cor = null;
        }
    }
}
