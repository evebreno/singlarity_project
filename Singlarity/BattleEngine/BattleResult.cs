﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.BattleEngine
{
    public enum BattleResult
    {
        /// <summary>
        /// Geralmente refere-se ao protagonista
        /// </summary>
        Player1Won = 1,
        /// <summary>
        /// Geralmente refere-se ao inimigo do protagonista
        /// </summary>
        player2Won = 2
    }
}
