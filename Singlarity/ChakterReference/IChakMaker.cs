﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.ChakterReference
{
    public interface IChakMaker
    {
        IChakter GenerateChak();
    }
}
