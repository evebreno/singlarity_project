﻿using System;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference.Behaviours;

namespace Singlarity.ChakterReference
{
    static public partial class ChakterCatalog
    {
        [VisibleChak(typeof(C_2))]
        private class C_2 : ChakterOrganism, IChakterCompleteReferenceData
        {
            public ChakterTipo Reference => ChakterTipo.wafungwa;
            public int Queries => 2;

            public ISepaInputTerminal[] Inputs
            {
                get
                {
                    ISepaInputTerminal[] inputs = new ISepaInputTerminal[3];
                    for (int i = 0; i < inputs.Length; i++)
                    {
                        inputs[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        inputs[i].Value = new SepaValue(GetValue(0.9625f, false), TypeOfSepaInputTerminal.Environment);
                    }

                    return inputs;
                }
            }
        }
    }
}
