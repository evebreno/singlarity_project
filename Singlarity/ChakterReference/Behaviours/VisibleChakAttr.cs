﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.ChakterReference.Behaviours
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    sealed class VisibleChakAttribute : Attribute
    {
        /// <summary>
        /// Faz a classe ser visível para o motor de busca do Chak
        /// </summary>
        /// <param name="Objeto"></param>
        public VisibleChakAttribute(Type Objeto)
        {
            _tipo = Objeto;
        }

        private Type _tipo;
        public Type Tipo { get { return _tipo; } }
    }
}
