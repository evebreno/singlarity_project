﻿using System;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference.Behaviours;

namespace Singlarity.ChakterReference
{
    static public partial class ChakterCatalog
    {
        [VisibleChak(typeof(C_32))]
        private class C_32 : ChakterOrganism, IChakterCompleteReferenceData
        {
            public ChakterTipo Reference => ChakterTipo.inlewe;
            public int Queries => 5;

            public ISepaInputTerminal[] Inputs
            {
                get
                {
                    ISepaInputTerminal[] inputs = new ISepaInputTerminal[4];
                    for (int i = 0; i < inputs.Length; i++)
                    {
                        inputs[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        inputs[i].Value = new SepaValue(GetValue(0.8349f, true), TypeOfSepaInputTerminal.Environment);
                    }

                    return inputs;
                }
            }
        }
    }
}
