﻿using System;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference.Behaviours;

namespace Singlarity.ChakterReference
{
    static public partial class ChakterCatalog
    {
        [VisibleChak(typeof(C_4))]
        private class C_4 : ChakterOrganism, IChakterCompleteReferenceData
        {
            public ChakterTipo Reference => ChakterTipo.jäätelööä;
            public int Queries => 3;

            public ISepaInputTerminal[] Inputs
            {
                get
                {
                    ISepaInputTerminal[] inputs = new ISepaInputTerminal[6];
                    for (int i = 0; i < inputs.Length; i++)
                    {
                        inputs[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        inputs[i].Value = new SepaValue(GetValue(0.5435f, true), TypeOfSepaInputTerminal.Environment);
                    }

                    return inputs;
                }
            }
        }
    }
}
