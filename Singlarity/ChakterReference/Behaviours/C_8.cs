﻿using System;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference.Behaviours;
using System.Security.Cryptography;

namespace Singlarity.ChakterReference
{
    static public partial class ChakterCatalog
    {
        [VisibleChak(typeof(C_8))]
        private class C_8 : ChakterOrganism, IChakterCompleteReferenceData
        {
            public ChakterTipo Reference => ChakterTipo.teksteillään;
            public int Queries => 7;

            public ISepaInputTerminal[] Inputs
            {
                get
                {
                    ISepaInputTerminal[] inputs = new ISepaInputTerminal[13];
                    for (int i = 0; i < inputs.Length; i++)
                    {
                        inputs[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        inputs[i].Value = new SepaValue(GetValue(0.51f, true), TypeOfSepaInputTerminal.Environment);
                    }

                    return inputs;
                }
            }
        }
    }
}
