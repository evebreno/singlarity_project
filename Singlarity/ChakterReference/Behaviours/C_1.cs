﻿using System;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference.Behaviours;

namespace Singlarity.ChakterReference
{
    static public partial class ChakterCatalog
    {
        [VisibleChak(typeof(C_1))]
        private class C_1 : ChakterOrganism, IChakterCompleteReferenceData
        {
            public ChakterTipo Reference => ChakterTipo.versbundel;
            public int Queries => 4;

            public ISepaInputTerminal[] Inputs
            {
                get
                {
                    ISepaInputTerminal[] inputs = new ISepaInputTerminal[5];
                    for(int i=0;i<inputs.Length;i++)
                    {
                        inputs[i] = ServiceLocator<ISepaInputTerminal>.SearchClass();
                        inputs[i].Value = new SepaValue(GetValue(0.75f, true), TypeOfSepaInputTerminal.Environment);
                    }

                    return inputs;
                }
            }
        }
    }
}
