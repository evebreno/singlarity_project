﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Singlarity.ChakterReference.Behaviours
{
    public class ChakterOrganism
    {
        private Random num = new Random();
        protected virtual bool GetValue(float percentage, bool valorEsperado)
        {
            return (num.NextDouble() < percentage) ? valorEsperado : !valorEsperado;
        }
    }
}
