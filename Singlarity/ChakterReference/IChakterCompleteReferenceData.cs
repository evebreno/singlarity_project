﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.SepaEngine;

namespace Singlarity.ChakterReference
{
    public interface IChakterCompleteReferenceData
    {
        ChakterTipo Reference { get; }
        ISepaInputTerminal[] Inputs { get; }
        /// <summary>
        /// Número de consultas antes de alterar seu valor
        /// </summary>
        int Queries { get; }
    }
}
