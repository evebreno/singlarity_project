﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameThor;
using Singlarity.SepaEngine;

namespace Singlarity.ChakterReference
{
    [Localizable(typeof(IChakMaker))]
    public class ChakMaker : IChakMaker
    {
        private Random r = new Random();
        public virtual IChakter GenerateChak()
        {
            IChakter chak = ServiceLocator<IChakter>.SearchClass();
            chak.Def = GenerateName();
            chak.Descricao = "";
            chak.Tipo = (ChakterTipo)Math.Pow(2, r.Next(6));
            chak.Sepa = GenerateSepa(chak.Tipo);

            return chak;
        }

        private string GenerateName()
        {
            IName name = NameThorServiceLocator.SearchNameGenerator();
            return name.GenerateName(1);
        }

        private byte[] GenerateSepa(ChakterTipo usedChakter)
        {
            byte[] buffer = GetGenericChain(0);
            NormalizeChain(buffer, usedChakter);
            byte[] validBytes = ValidadeChain(buffer, usedChakter);

            return validBytes;
        }

        /// <summary>
        /// Obtém uma cadeia de Sepas genéricas, não normalizada nem verificada
        /// </summary>
        /// <param name="Size">Tamanho da cadeia (0 para gerar um tamanho aleatório de até 64 bytes)</param>
        private byte[] GetGenericChain(int size)
        {
            int chainSize = (size == 0) ? r.Next(1, 65) : size;

            byte[] buffer = new byte[chainSize];
            r.NextBytes(buffer);
            return buffer;
        }

        /// <summary>
        /// Altera a cadeia de bytes para uma cadeia válida para determinado Chakter
        /// </summary>
        /// <param name="chainToNormalize">Cadeia a ser normalizada</param>
        /// <param name="usedChakter">Tipo de chakter a ser usado no chip</param>
        /// <returns>Cadeia corretamente normalizada sem verificação de módulo</returns>
        private void NormalizeChain(byte[] chainToNormalize, ChakterTipo usedChakter)
        {
            for (int i = 0; i < chainToNormalize.Length; i++)
            {
                while (!SepaCatalog.CheckExistence(chainToNormalize[i], usedChakter))
                {
                    chainToNormalize[i] = (byte)r.Next(256);
                }
            }
        }

        /// <summary>
        /// Valida a cadeia de bytes tentando transformar os módulos incorretos em módulos válidos
        /// </summary>
        /// <param name="chainToValidade">Cadeia a ser validada</param>
        /// <param name="usedChakter">Chakter utilizado</param>
        /// <returns>Cadeia com sua modulação válida e coerente com o tipo de Chakter</returns>
        private byte[] ValidadeChain(byte[] chainToValidade, ChakterTipo usedChakter)
        {
            bool redoChain = false;
            int lastError = -1;
            int timestrying = 0;
            List<byte> newChain = new List<byte>();
            byte[] modifiedChain = chainToValidade.Clone() as byte[];

            while (!SepaCatalog.CheckModuleConsistence(modifiedChain, usedChakter, out int error))
            {
                if (lastError == error) { timestrying++; }
                else { lastError = error; timestrying = 0; }

                if (timestrying >= 64) { redoChain = true; break; }

                byte[] incommingByte = new byte[1];
                r.NextBytes(incommingByte);

                NormalizeChain(incommingByte, usedChakter);
                modifiedChain[error] = incommingByte[0];
            }

            if (redoChain)
            {
                for (int i = 0; i < lastError; i++)
                { newChain.Add(modifiedChain[i]); }
            }
            else newChain = modifiedChain.ToList();

            return newChain.ToArray();
        }
    }
}
