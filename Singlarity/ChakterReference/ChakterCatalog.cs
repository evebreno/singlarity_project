﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Singlarity.SepaEngine;
using Singlarity.ChakterReference.Behaviours;

namespace Singlarity.ChakterReference
{
    static public partial class ChakterCatalog
    {
        static public IChakterCompleteReferenceData[] Chakters { get { if (!_gathererd) GatherReferences(); return _chakters.ToArray(); } }
        static private List<IChakterCompleteReferenceData> _chakters = new List<IChakterCompleteReferenceData>();

        static private bool _gathererd = false;
        static private void GatherReferences()
        {
            Type chakterCatalog = typeof(ChakterCatalog);
            MemberInfo[] membros = chakterCatalog.FindMembers(MemberTypes.NestedType, BindingFlags.NonPublic, new MemberFilter(DelegateToSearchCriteria), typeof(IChakterCompleteReferenceData));

            foreach (var membro in membros)
            {
                VisibleChakAttribute attr = membro.GetCustomAttribute<VisibleChakAttribute>();
                Type m = attr.Tipo;
                ConstructorInfo construct = m.GetConstructor(Type.EmptyTypes);

                _chakters.Add(construct.Invoke(new object[] { }) as IChakterCompleteReferenceData);
            }

            _gathererd = true;
        }

        private static bool DelegateToSearchCriteria(MemberInfo objMemberInfo, Object objSearch)
        {
            Type memb = objMemberInfo.GetType();
            VisibleChakAttribute attr = objMemberInfo.GetCustomAttribute<VisibleChakAttribute>();

            if (attr != null) return true;

            return false;
        }

        static public int GetEnvironmentInputs(ChakterTipo tipo)
        {
            foreach (var chak in Chakters)
            {
                if (chak.Reference == tipo) { return chak.Inputs.Length; }
            }

            throw new Exception("Não foi possível encontrar a referência!");
        }
        static public ISepaInputTerminal[] GetEnvironmentInputsValue(ChakterTipo tipo)
        {
            foreach (var chak in Chakters)
            {
                if (chak.Reference == tipo) { return chak.Inputs; }
            }

            throw new Exception("Não foi possível encontrar a referência!");
        }
        static public int GetQueryTimes(ChakterTipo tipo)
        {
            foreach (var chak in Chakters)
            {
                if (chak.Reference == tipo) { return chak.Queries; }
            }

            throw new Exception("Não foi possível encontrar a referência!");
        }
    }
}
