﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity;

namespace Singlarity.User
{
    public class Player : IUser
    {
        public string Nome { get { return _nome; } set { _nome = value; } }
        public float Coin { get => _coin; set => _coin = value; }
        public float CreditMargin { get => _creditMargin; set => _creditMargin = value; }
        public Chip[] Chips { get => _chipes; set => _chipes = value; }
        public float Life { get => _life; set => _life = value; }

        private string _nome;
        private float _coin;
        private float _creditMargin;
        private Chip[] _chipes;
        private float _life;

        public Player(string nome)
        {
            _nome = nome;
            _coin = 150;
            _creditMargin = 0.01f;
            _chipes = new Chip[0];
            _chipes.Initialize();
            _life = 100;
        }

        public Player(IUser user)
        {
            _nome = user.Nome;
            _coin = user.Coin;
            _creditMargin = user.CreditMargin;
            _chipes = user.Chips;
            _life = user.Life;
        }

        static public IUser GetPlayer(string nome, float coin, float margin, Chip[] chips, float life)
        {
            Player np = new Player(nome);
            np.Coin = coin;
            np.CreditMargin = margin;
            np.Chips = chips;
            np.Life = life;

            return np;
        }
    }
}
