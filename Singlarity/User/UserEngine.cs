﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity;
using Singlarity.Files;

namespace Singlarity.User
{
    [RequiresInitialization("SetPresentation")]
    public class UserEngine
    {
        static public void SetPresentation()
        {
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.SetUser_f, Commands_PTBR.SetUser_d, new MainBoardFunctionDelegation(typeof(UserEngine), "SetUser") }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Status_f, Commands_PTBR.Status_d, new MainBoardFunctionDelegation(typeof(UserEngine), "ShowStatus") }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Chip_f, Commands_PTBR.Chip_d, new MainBoardFunctionDelegation(typeof(UserEngine), "ShowChip") }));

            GetUser();
        }

        public static void SetUser()
        {
            if (!Protagonista.HasUser)
            {
                Log.ShowWarning("Nome do Usuário: ");
                string nome = Console.ReadLine();
                Player p = new Player(nome);
                Protagonista.AddUser(p);

                UserFile.SaveUserData(Protagonista.GetUser());
            }

            else Log.ShowWarningLine("Já existe um usuário definido, não é possível criar outro!");
        }
        public static void ShowStatus()
        {
            try
            {
                Log.ShowLine($"Nome: {Protagonista.Nome}");
                //Log.ShowLine($"Chipes: {MainBoard.Usuario.Chipes.Length}");
                Log.ShowLine($"Dinheiro: ${Protagonista.Coin} glty");
                Log.ShowDetailLine($"Margem de crédito: {Protagonista.Credit * 100}%");
                Log.ShowLine($"Saúde: {Protagonista.Life}");
            }
            catch (NullReferenceException)
            { Log.ShowWarningLine("Não há usuário definido!"); }
        }
        public static void ShowChip()
        {
            try
            {
                foreach(var chip in Protagonista.Chips)
                {
                    Console.BackgroundColor = chip.Cor.BackColor;
                    Console.ForegroundColor = chip.Cor.ForeColor;
                    Console.WriteLine($"{chip.Nome}\t{chip.Signo}\tChakters: {chip.Carac.Length}");
                }
            }
            catch (NullReferenceException)
            { Log.ShowWarningLine("Não foi encontrado chip algum"); }
            finally { Console.ResetColor(); }
        }
        private static void GetUser()
        {
            IUser tmpuser = null;
            try
            {
                UserFile.FileToRead = ServiceLocator<IReadFile>.SearchClass();
                tmpuser = UserFile.RetrieveUserData();
            }
            catch { Log.ShowWarningLine($"Não há um usuário definido! \nÉ necessário um usuário, tente o comando: {Commands_PTBR.SetUser_f}"); }

            try
            {
                ChipFile.FileToRead = ServiceLocator<IReadFile>.SearchClass();
                tmpuser.Chips = ChipFile.RetriveChipData();
            }
            catch { }
            finally
            {
                Protagonista.AddUser(tmpuser);
            }
        }

        static public bool SaveProtagonistData()
        {
            try
            {
                UserFile.SaveUserData(Protagonista.GetUser());
                ChipFile.SaveChipsData(Protagonista.Chips);
                return true;
            }
            catch { return false; }
        }
    }
}
