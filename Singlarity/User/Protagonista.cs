﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity;

namespace Singlarity.User
{
    [RequiresInitialization("InitializeProtagonist", InitializationOrder = 9)]
    static public class Protagonista
    {
        static public string Nome => _myPlayer.Nome;
        static public float Life => _myPlayer.Life;
        static public float Coin => _myPlayer.Coin;
        static public float Credit => _myPlayer.CreditMargin;
        static public Chip[] Chips => _myPlayer.Chips;
        public static bool HasUser => _hasUser;

        static private IUser _myPlayer;
        static private bool _hasUser;
        
        static public void InitializeProtagonist()
        {
            string s = null;
            _myPlayer = new Player(s);
        }

        static public void AddUser(IUser arrives)
        {
            _myPlayer = new Player(arrives);
            _hasUser = true;
        }

        /// <summary>
        /// Obtém uma cópia das informações do usuário, mas não a referencia
        /// </summary>
        static public IUser GetUser()
        {
            return new Player(_myPlayer);
        }

        static public void AddCoin(float value)
        {
            _myPlayer.Coin += value;
            SaveData();
        }
        static public void RemoveCoin(float value)
        {
            _myPlayer.Coin -= value;
            SaveData();
        }

        static public void AddChip(Chip arrivingChip)
        {
            List<Chip> chips = new List<Chip>();
            try { chips.AddRange(_myPlayer.Chips); }
            catch (ArgumentNullException) { _myPlayer.Chips = new Chip[0]; }
            finally
            {
                chips.Add(arrivingChip);
                _myPlayer.Chips = chips.ToArray();
                SaveData();
            }
        }

        //TODO: Acrescentar um evento de mudança de dados se houver necessidade.
        static private void SaveData()
        {
            UserEngine.SaveProtagonistData();
        }
    }
}
