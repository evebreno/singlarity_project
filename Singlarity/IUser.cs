﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public interface IUser
    {
        string Nome { get; set; }
        float Coin { get; set; }
        /// <summary>
        /// Margem de crédito ao usuário, caso entre em dívida
        /// </summary>
        float CreditMargin { get; set; }
        Chip[] Chips { get; set; }
        /// <summary>
        /// Vida do usuário. Tende a perdê-la em batalhas perdidas
        /// </summary>
        float Life { get; set; }
        //TODO: Incluir talvez um userpref
    }
}
