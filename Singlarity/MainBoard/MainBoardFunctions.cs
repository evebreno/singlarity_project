﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public interface MainBoardFunctions
    {
        string Function { get; set; }
        string Definition { get; set; }
    }
}
