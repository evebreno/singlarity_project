﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.User;
using System.Reflection;

namespace Singlarity
{
    [RequiresInitialization("Initialize", InitializationOrder = 255, InitializationTime = RequiresInitializationAttribute.InitializationPosition.PostInitialization)]
    static public partial class MainBoard
    {
        static public List<IMainBoardFunction> Functions { get => functions; }
        static private List<IMainBoardFunction> functions = new List<IMainBoardFunction>();

        static public void Initialize()
        {
            Commands();
        }

        static void Commands()
        {
            while (true)
            {
                try { Log.Show($"{Protagonista.Nome}->"); }
                catch { Log.Show($"{Environment.UserName}->"); }

                string comand = Console.ReadLine().ToLower();
                CallCommand(comand);
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Adiciona uma função ao MainBoard
        /// </summary>
        /// <param name="function">Função a ser adicionada</param>
        static public void AddFunction(IMainBoardFunction function)
        {
            functions.Add(function);
        }
        static private void CallCommand(string command)
        {
            foreach(var func in functions)
            {
                if(func.Function == command)
                {
                    Type tipo = func.Delegation.Classe;
                    MethodInfo met = tipo.GetMethod(func.Delegation.Metodo);

                    met.Invoke(tipo, func.Delegation.Arguments);
                }
            }
        }
    }
}