﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public class AmbiguosCommandException : Exception
    {
        public override string Message => _message;
        private string _message;

        public AmbiguosCommandException()
        {
            _message = "";
        }
        public AmbiguosCommandException(string message)
        {
            _message = message;
        }
    }
}
