﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    [RequiresInitialization("SetPresentation")]
    sealed class MainBoardUtility
    {
        static public void SetPresentation()
        {
            //MainBoard.GatheringFunctions += MainBoard_GatheringFunctions;
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Commands_f, Commands_PTBR.Commands_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Listar") }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Cls_f, Commands_PTBR.Cls_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Limpar") }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { Commands_PTBR.Exit_f, Commands_PTBR.Exit_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Sair") }));
#region Funções Complementares
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "help", Commands_PTBR.Commands_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Listar", true) }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "ls", Commands_PTBR.Commands_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Listar", true) }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "cls", Commands_PTBR.Cls_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Limpar", true) }));
            MainBoard.AddFunction(ServiceLocator<IMainBoardFunction>.SearchClass(null, new object[] { "exit", Commands_PTBR.Exit_d, new MainBoardFunctionDelegation(typeof(MainBoardUtility), "Sair", true) }));
#endregion
        }

        static public void Listar()
        {
            foreach(var functions in MainBoard.Functions)
            {
                if(!functions.Delegation.Hidden)
                {
                    Log.ShowLine($"{functions.Function}\t\t{functions.Definition}");
                }
            }
        }
        static public void Limpar()
        {
            Console.Clear();
        }
        static public void Sair()
        {
            Environment.Exit(0);
        }

        #region Obsoleto
        //static private void MainBoard_GatheringFunctions(object sender, EventArgs e)
        //{
        //    MainBoard.AddFunction(new FunctionToMainBoard(Commands_PTBR.Commands_f, Commands_PTBR.Commands_d));
        //    MainBoard.AddFunction(new FunctionToMainBoard(Commands_PTBR.Cls_f, Commands_PTBR.Cls_d));
        //    MainBoard.AddFunction(new FunctionToMainBoard(Commands_PTBR.Exit_f, Commands_PTBR.Exit_d));

        //    MainBoard.CallingFunction += MainBoard_CallingFunction;
        //}

        //static private void MainBoard_CallingFunction(string especificFunction)
        //{
        //    if (especificFunction == Commands_PTBR.Commands_f || especificFunction == "help" || especificFunction == "ls")
        //    {
        //        foreach (MainBoardFunctions mbf in MainBoard.Functions) Log.ShowLine($"{mbf.Function}\t\t{mbf.Definition}");
        //    }

        //    else if (especificFunction == Commands_PTBR.Cls_f || especificFunction == "cls") Console.Clear();
        //    else if (especificFunction == Commands_PTBR.Exit_f || especificFunction == "exit") Environment.Exit(0);
        //}
        #endregion


    }
}
