﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public interface IMainBoardFunction : MainBoardFunctions
    {
        new string Function { get; set; }
        new string Definition { get; set; }
        MainBoardFunctionDelegation Delegation { get; set; }
    }

    public struct MainBoardFunctionDelegation
    {
        public Type Classe { get; set; }
        public string Metodo { get; set; }
        public string[] Arguments { get; set; }
        public bool Hidden { get; private set; }

        public MainBoardFunctionDelegation(Type classe, string metodo, params string[] args)
        {
            Classe = classe;
            Metodo = metodo;
            Arguments = args;
            Hidden = false;
        }
        public MainBoardFunctionDelegation(Type classe, string metodo, bool hidden, params string[] args)
        {
            Classe = classe;
            Metodo = metodo;
            Arguments = args;
            Hidden = hidden;
        }
    }
}
