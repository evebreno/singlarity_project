﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity
{
    public class FunctionToMainBoard : MainBoardFunctions
    {
        public string Function { get { return _function; } set { _function = value.ToLower(); } }
        public string Definition { get { return _defintion; } set { _defintion = value.ToLower(); } }

        private string _function = "";
        private string _defintion = "";

        public FunctionToMainBoard(string function, string definition)
        {
            _function = function.ToLower();
            _defintion = definition.ToLower();
        }
    }
}
