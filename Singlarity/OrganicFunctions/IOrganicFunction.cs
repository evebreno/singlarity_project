﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.OrganicFunctions.Commands;

namespace Singlarity.OrganicFunctions
{
    public interface IOrganicFunction
    {
        ComplexRule[] CompleteRules { get; }
    }
}
