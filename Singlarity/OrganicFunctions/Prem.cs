﻿using System.Collections.Generic;
using Singlarity.OrganicFunctions.Commands;

namespace Singlarity.OrganicFunctions
{
    static public class Prem
    {
        static public ComplexRule[] CompleteRules
        {
            get
            {
                return new ComplexRule[] { R1, R2, R3, R4 };
            }
        }
        
        static private ComplexRule R1 //O-Alder->Indigo-Xn-#
        {
            get
            {
                List<FunctionCommand> comandos = new List<FunctionCommand>
                {
                    new FunctionCommand(new GenericRule(), LogicPosition.Start, SepaEngine.GenreType.Alder),
                    new FunctionCommand(new GenericRule(), LogicPosition.None, SepaEngine.GenreType.Indigo),
                    new FunctionCommand(new GenericRule(0, GenericRule.GenericModuleType.Neutro), LogicPosition.End, SepaEngine.GenreType.Neutro)
                };
                return new ComplexRule(comandos);
            }
        }
        static private ComplexRule R2 //O-Alder-Xn*-Indigo-XN-#
        {
            get
            {
                List<FunctionCommand> comandos = new List<FunctionCommand>
                {
                    new FunctionCommand(new GenericRule(), LogicPosition.Start, SepaEngine.GenreType.Alder),
                    new FunctionCommand(new GenericRule(1, GenericRule.GenericModuleType.Neutro), LogicPosition.None, SepaEngine.GenreType.Neutro),
                    new FunctionCommand(new GenericRule(), LogicPosition.None, SepaEngine.GenreType.Indigo),
                    new FunctionCommand(new GenericRule(0, GenericRule.GenericModuleType.Neutro), LogicPosition.End, SepaEngine.GenreType.Neutro)
                };
                return new ComplexRule(comandos);
            }
        }
        static private ComplexRule R3 //O-Alder-Yn-Alder-Yn-#
        {
            get
            {
                List<FunctionCommand> comandos = new List<FunctionCommand>
                {
                    new FunctionCommand(new GenericRule(), LogicPosition.Start, SepaEngine.GenreType.Alder),
                    new FunctionCommand(new GenericRule(0, GenericRule.GenericModuleType.Neutro | GenericRule.GenericModuleType.Tipado), LogicPosition.None, SepaEngine.GenreType.Neutro),
                    new FunctionCommand(new GenericRule(), LogicPosition.None, SepaEngine.GenreType.Alder),
                    new FunctionCommand(new GenericRule(0, GenericRule.GenericModuleType.Neutro | GenericRule.GenericModuleType.Tipado), LogicPosition.End, SepaEngine.GenreType.Neutro),
                };
                return new ComplexRule(comandos);
            }
        }
        static private ComplexRule R4 //O-Indigo-Yn*-#
        {
            get
            {
                List<FunctionCommand> comandos = new List<FunctionCommand>
                {
                    new FunctionCommand(new GenericRule(), LogicPosition.Start, SepaEngine.GenreType.Indigo),
                    new FunctionCommand(new GenericRule(1, GenericRule.GenericModuleType.Neutro | GenericRule.GenericModuleType.Tipado), LogicPosition.End, SepaEngine.GenreType.Neutro)
                };

                return new ComplexRule(comandos);
            }
        }
    }
}