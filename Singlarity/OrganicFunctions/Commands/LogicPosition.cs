﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.OrganicFunctions.Commands
{
    public enum LogicPosition
    {
        None = 0,
        Start = 1,
        End = 2
    }
}
