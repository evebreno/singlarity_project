﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.OrganicFunctions.Commands
{
    public class ComplexRule
    {
        public IList<FunctionCommand> Rule { get; set; }

        public ComplexRule()
        {
            Rule = new List<FunctionCommand>();
        }
        
        public ComplexRule(IList<FunctionCommand> rules)
        {
            Rule = rules;
        }
    }
}
