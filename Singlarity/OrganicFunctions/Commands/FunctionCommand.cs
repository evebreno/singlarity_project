﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.SepaEngine;

namespace Singlarity.OrganicFunctions.Commands
{
    public class FunctionCommand
    {
        public GenericRule Generic { get; }
        public LogicPosition Position { get; }
        public GenreType Genre { get; }
        public uint MinInputs { get; }

        public FunctionCommand(GenericRule regra, LogicPosition posicao, GenreType genero)
        {
            Generic = regra;
            Position = posicao;
            Genre = genero;
            MinInputs = 1;
        }

        public FunctionCommand(GenericRule regra, LogicPosition posicao, GenreType genero, uint inputs)
        {
            Generic = regra;
            Position = posicao;
            Genre = genero;
            MinInputs = inputs;
        }
    }
}
