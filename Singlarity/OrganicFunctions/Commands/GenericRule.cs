﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlarity.OrganicFunctions.Commands
{
    public struct GenericRule
    {
        /// <summary>
        /// Valor mínimo [0: qualquer valor], menor que 0: valor fixo
        /// </summary>
        public int Min { get; }
        public GenericModuleType ModuleType { get; }

        public GenericRule (int minValue, GenericModuleType genericModule)
        {
            Min = minValue;
            ModuleType = genericModule;
        }

        [Flags]
        public enum GenericModuleType { Neutro=1,Tipado =2 };
    }
}
