﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singlarity.OrganicFunctions.Commands;

namespace Singlarity.OrganicFunctions
{
    static public class Diza
    {
        static public ComplexRule[] CompleteRules
        {
            get
            {
                return new ComplexRule[] { R1, R2, R3, R4 };
            }
        }

        static private ComplexRule Fundamental //O-Xn*-Vertice-|Yn*-Nupla-Xn-#
        {
            get
            {
                List<FunctionCommand> comandos = new List<FunctionCommand>
                {
                    new FunctionCommand(new GenericRule(1, GenericRule.GenericModuleType.Neutro), LogicPosition.Start, SepaEngine.GenreType.Neutro),
                    new FunctionCommand(new GenericRule(), LogicPosition.None, SepaEngine.GenreType.Vertice),
                    new FunctionCommand(new GenericRule(1, GenericRule.GenericModuleType.Tipado), LogicPosition.None, SepaEngine.GenreType.Neutro),
                    new FunctionCommand(new GenericRule(), LogicPosition.None, SepaEngine.GenreType.Nupla),
                    new FunctionCommand(new GenericRule(0, GenericRule.GenericModuleType.Neutro), LogicPosition.End, SepaEngine.GenreType.Neutro),
                };
                return new ComplexRule(comandos);
            }
        }
    }
}
